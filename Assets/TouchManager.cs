﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManager : MonoBehaviour
{
    public float hitUp = 500f;
    public float hitLeft = 1000f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;
            if (ReturnClickedObject(out hitInfo, Camera.main) != null)
            {
                if (ReturnClickedObject(out hitInfo, Camera.main).GetComponent<Wood>())
                {
                    ReturnClickedObject(out hitInfo, Camera.main).GetComponent<Rigidbody>().isKinematic = false;
                    ReturnClickedObject(out hitInfo, Camera.main).GetComponent<Rigidbody>().useGravity = true;
                    ReturnClickedObject(out hitInfo, Camera.main).GetComponent<Rigidbody>().AddForce(Vector3.up * hitUp);
                    ReturnClickedObject(out hitInfo, Camera.main).GetComponent<Rigidbody>().AddForce(Vector3.right * hitLeft);
                    //ReturnClickedObject(out hitInfo, Camera.main).tag = "Death";
                    ReturnClickedObject(out hitInfo, Camera.main).GetComponent<Wood>().active = false;
                    //Destroy(ReturnClickedObject(out hitInfo, Camera.main), 3f);
                }
            }
        }
    }
    GameObject ReturnClickedObject(out RaycastHit hit, Camera TemCam)
    {
        GameObject target = null;
        Ray ray = TemCam.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray.origin, ray.direction * 10, out hit))
        {
            target = hit.collider.gameObject;
        }
        return target;
    }
}
