﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DiceManager : Singleton<DiceManager>
{
    public List<PlayerRef> players;
    public GameObject buttons;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void RefreshPlayers()
    {
        players = GetComponentsInChildren<PlayerRef>().ToList();
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void ActivateCamera(Player playerToMove)
    {
        buttons.SetActive(true);
        PlayerRef playerChoosen = players.Where(p => p.player == playerToMove).ToArray()[0];
        PlayerManager.main.playerOnTurn = playerChoosen.gameObject;
        pabloReference.main.pablo = playerChoosen.gameObject;
    }
    public void RollDice(Player playerToMove, int diceNumber)
    {
        PlayerRef playerChoosen = players.Where(p => p.player == playerToMove).ToArray()[0];
        playerChoosen.MovePlayer(SectionManager.main.SectionToMove(players.Where(p => p.player == playerToMove).ToArray()[0], diceNumber));
    }
    public void MoveTo(Player playerToMove, int sectionID)
    {
        PlayerRef playerChoosen = players.Where(p => p.player == playerToMove).ToArray()[0];
        Debug.LogError("specialsection");
        playerChoosen.MovePlayer(SectionManager.main.SectionToMoveOnSpecialSection(players.Where(p => p.player == playerToMove).ToArray()[0], sectionID));
    }
}
