﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class IncomingDataPlayer 
{
    public string hostId;
    public string playerId;
    public string nameId;
    public string profilePic;
    public int posOnBoard;
    public int diceNumber;
}
