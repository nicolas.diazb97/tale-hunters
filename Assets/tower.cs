﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tower : MonoBehaviour
{
    public Material opaque;
    public Material transparent;
    public MeshRenderer mesh;
    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ChangeMaterialToOpaque()
    {
        Material[] mats = new Material[] {
        opaque
        };
       mesh.GetComponent<MeshRenderer>().materials = mats;
    }
    public void ChangeMaterialToTransparent()
    {
        Material[] mats = new Material[] {
        transparent
        };
        mesh.GetComponent<MeshRenderer>().materials = mats;
    }
}
