﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultDice : MonoBehaviour
{
    public Animator result;
    public float animationSpeed = 1;
    public bool number1 = false;
    public bool number2 = false;
    public bool number3 = false;
    public bool number4 = false;

    public Text resultado1;
    public Text resultado2;
    public Text resultado3;
    public Text resultado4;

    public Dadosminijuego dice1;
    public SignDice opDice;
    public Dice2 dice2;

    public float finalResult = 0;

    public float sumaResult = 0;
    public float restaResult = 0;
    public float multiResult = 0;
    public float divResult = 0;
    public float random1;
    public float random2;
    public float random4;

    public GameObject win;
    public GameObject lose;
    SocketManager socketManager;
    private void Start()
    {
        socketManager = FindObjectOfType<SocketManager>();
    }
    public void tryToStop()
    {
        socketManager.RollMiniGameDice();
    }
    public void Results()
    {
        if (opDice.signResult == 1)
        {
            sumaResult = dice1.numberResult + dice2.dice2Result;
            resultado3.text = sumaResult.ToString();
            random1 = sumaResult + Random.Range(1, 10);
            random2 = sumaResult + Random.Range(1, 10);
            random4 = sumaResult + Random.Range(1, 10);
            resultado1.text = random1.ToString();
            resultado2.text = random2.ToString();
            resultado4.text = random4.ToString();
        }
        if (opDice.signResult == 2)
        {
            restaResult = dice1.numberResult - dice2.dice2Result;
            resultado3.text = restaResult.ToString();
            random1 = restaResult - Random.Range(2, 5);
            random2 = restaResult - Random.Range(1, 3);
            random4 = restaResult - Random.Range(1, 4);
            resultado1.text = random1.ToString();
            resultado2.text = random2.ToString();
            resultado4.text = random4.ToString();
        }
        if (opDice.signResult == 3)
        {
            multiResult = dice1.numberResult * dice2.dice2Result;
            resultado3.text = multiResult.ToString();
            random1 = multiResult - Random.Range(2, 5);
            random2 = multiResult - Random.Range(1, 5);
            random4 = multiResult - Random.Range(1, 4);
            resultado1.text = random1.ToString();
            resultado2.text = random2.ToString();
            resultado4.text = random4.ToString();

        }
        if (opDice.signResult == 4)
        {
            divResult = dice1.numberResult / dice2.dice2Result;
            resultado3.text = divResult.ToString();
            random1 = multiResult - Random.Range(1, 5);
            random2 = multiResult - Random.Range(1, 5);
            random4 = multiResult - Random.Range(1, 5);
            resultado1.text = random1.ToString();
            resultado2.text = random2.ToString();
            resultado4.text = random4.ToString();
        }
    }
    public void StopDice()
    {
        animationSpeed = result.speed = 0;
        if (number1 == true && animationSpeed == 0)
        {
            lose.SetActive(true);
            result.Play("+Up");
        }
        if (number2 == true && animationSpeed == 0)
        {
            lose.SetActive(true);
            result.Play("-Up");
        }
        if (number3 == true && animationSpeed == 0)
        {
            win.SetActive(true);
            result.Play("xUp");
        }
        if (number4 == true && animationSpeed == 0)
        {
            lose.SetActive(true);
            result.Play("dUp");
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void number1On()
    {
        number1 = true;
    }

    public void number1Off()
    {
        number1 = false;
    }

    public void number2On()
    {
        number2 = true;
    }

    public void number2Off()
    {
        number2 = false;
    }

    public void number3On()
    {
        number3 = true;
    }

    public void number3Off()
    {
        number3 = false;
    }

    public void number4On()
    {
        number4 = true;
    }

    public void number4Off()
    {
        number4 = false;
    }

    public void GrowToAnimation()
    {
        result.Play("ResultMoves");
    }
}
