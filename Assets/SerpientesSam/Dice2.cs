﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice2 : MonoBehaviour
{
    public Animator dice2;
    public Animator result;
    public float animationSpeed = 1;
    public bool number3 = false;
    public bool number4 = false;
    public bool number5 = false;
    public bool number6 = false;

    public ResultDice results;
    public float dice2Result = 0;
    SocketManager socketManager;
    private void Start()
    {
        socketManager = FindObjectOfType<SocketManager>();
    }
    public void tryToStop()
    {
        socketManager.RollMiniGameDice();
    }
    public void StopDice()
    {
        animationSpeed = dice2.speed = 0;
        if (number3 == true && animationSpeed == 0)
        {
            dice2Result = 3;
            dice2.Play("+Up");
            result.Play("Grow");
            results.Results();
        }
        if (number4 == true && animationSpeed == 0)
        {
            dice2Result = 4;
            dice2.Play("-Up");
            result.Play("Grow");
            results.Results();
        }
        if (number5 == true && animationSpeed == 0)
        {
            dice2Result = 5;
            dice2.Play("xUp");
            result.Play("Grow");
            results.Results();
        }
        if (number6 == true && animationSpeed == 0)
        {
            dice2Result = 6;
            dice2.Play("dUp");
            result.Play("Grow");
            results.Results();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void number3On()
    {
        number3 = true;
    }

    public void number3Off()
    {
        number3 = false;
    }

    public void number4On()
    {
        number4 = true;
    }

    public void number4Off()
    {
        number4 = false;
    }

    public void number5On()
    {
        number5 = true;
    }

    public void number5Off()
    {
        number5 = false;
    }

    public void number6On()
    {
        number6 = true;
    }

    public void number6Off()
    {
        number6 = false;
    }

    public void GrowToAnimation()
    {
        dice2.Play("Dice2Moves");
    }
}
