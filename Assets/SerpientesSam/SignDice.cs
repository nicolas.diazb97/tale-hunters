﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignDice : MonoBehaviour
{
    public Animator signs;
    public Animator dice2;
    public float animationSpeed = 1;
    public bool signplus = false;
    public bool signminus = false;
    public bool signTimes = false;
    public bool signDivide = false;

    public float signResult = 0;

    SocketManager socketManager;
    private void Start()
    {
        socketManager = FindObjectOfType<SocketManager>();
    }
    public void tryToStop()
    {
        socketManager.RollMiniGameDice();
    }
    public void StopDice()
    {
        animationSpeed = signs.speed = 0;
        if (signplus == true && animationSpeed == 0)
        {
            signResult = 1;
            signs.Play("+Up");
            dice2.Play("Grow");
        }
        if (signminus == true && animationSpeed == 0)
        {
            signResult = 2;
            signs.Play("-Up");
            dice2.Play("Grow");
        }
        if (signTimes == true && animationSpeed == 0)
        {
            signResult = 3;
            signs.Play("xUp");
            dice2.Play("Grow");
        }
        if (signDivide == true && animationSpeed == 0)
        {
            signResult = 4;
            signs.Play("dUp");
            dice2.Play("Grow");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SignplusOn()
    {
        signplus = true;
    }

    public void SignplusOff()
    {
        signplus = false;
    }

    public void SignminusOn()
    {
        signminus = true;
    }

    public void SignminusOff()
    {
        signminus = false;
    }

    public void signTimesOn()
    {
        signTimes = true;
    }

    public void signTimesOff()
    {
        signTimes = false;
    }

    public void signDivideOn()
    {
        signDivide = true;
    }

    public void signDivideOff()
    {
        signDivide = false;
    }

    public void GrowToAnimation()
    {
        signs.Play("SignDiceMove");
    }
}
