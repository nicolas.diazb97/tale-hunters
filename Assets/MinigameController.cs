﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MinigameController : Singleton<MinigameController>
{
    public GameObject[] characters;
    public Player[] players;
    public GameObject[] buttons;
    public int currTurn = 0;
    public GameObject holder;
    public bool initialized = false;
    bool rolled = false;
    SocketManager socketManager;

    // Start is called before the first frame update
    void Start()
    {
        socketManager = FindObjectOfType<SocketManager>();
    }
    public void Init(Player _playerToPlay)
    {
        if (!initialized)
        {
            holder.SetActive(true);
            players[3] = _playerToPlay;
            buttons[3].GetComponent<Button>().onClick.AddListener(() => socketManager.ClaimNewTurn2());
            var block = new MaterialPropertyBlock();
            int counter = 0;
            // You can look up the property by ID i[nstead of the string to be more efficient.
            block.SetColor("_BaseColor", PlayerManager.main.chipColors[_playerToPlay.consecutiveId]);

            List<Renderer> playerMesh = characters[3].GetComponentsInChildren<Renderer>().ToList();
            // You can cache a reference to the renderer to avoid searching for it.
            playerMesh.ForEach(pm => pm.SetPropertyBlock(block));
            for (int i = 0; i < characters.Length; i++)
            {
                if (players[3].consecutiveId != i)
                {
                    counter++;
                    if (counter == 1)
                    {
                        players[0] = PlayerManager.main.GetPlayerByConsecutiveId(i);
                    }
                    if (counter == 2)
                    {
                        players[1] = PlayerManager.main.GetPlayerByConsecutiveId(i);
                    }
                    if (counter == 3)
                    {
                        players[2] = PlayerManager.main.GetPlayerByConsecutiveId(i);
                    }
                    var block2 = new MaterialPropertyBlock();

                    // You can look up the property by ID i[nstead of the string to be more efficient.
                    block2.SetColor("_BaseColor", PlayerManager.main.chipColors[i]);

                    // You can cache a reference to the renderer to avoid searching for it.
                    List<Renderer> playerMeshes = characters[i].GetComponentsInChildren<Renderer>().ToList();
                    // You can cache a reference to the renderer to avoid searching for it.
                    playerMeshes.ForEach(pm => pm.SetPropertyBlock(block2));
                }
                initialized = true;
            }
            buttons.ToList().ForEach(b =>
            {
                b.GetComponent<Button>().interactable = false;
                b.SetActive(false);
            });
            Player activePlayer = PlayerManager.main.GetPlayerByConsecutiveId(PlayerManager.main.currId);
            for (int i = 0; i < buttons.Length; i++)
            {
                if (activePlayer == players[i])
                    buttons[i].SetActive(true);
            }
            SetNewMiniGameTurn();
        }
    }

    public void RollDice()
    {
        if (!rolled &&currTurn<=3)
        {
            rolled = true;
            buttons[currTurn].GetComponent<MiniGameDice>().VirtualClicked.Invoke();
            currTurn++;
            SetNewMiniGameTurn();
        }
    }

    public void SetNewMiniGameTurn()
    {
        Debug.LogError("currturn: " + currTurn);
        buttons[currTurn].GetComponent<Button>().interactable = true;
        rolled = false;
    }
    // Update is called once per frame
    void Update()
    {

    }
}
