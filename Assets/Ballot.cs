﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ballot : MonoBehaviour
{
    // Start is called before the first frame update
    public void PlayLetterSound()
    {
        BallotsManager.main.PlaySound(GetComponentInChildren<BallotLetter>().GetComponentInChildren<Text>().text);
    }
    public void PlayNumberSound()
    {
        BallotsManager.main.PlaySound(GetComponentInChildren<BallotNumber>().GetComponentInChildren<Text>().text);
    }
}
