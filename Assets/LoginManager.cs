﻿using Facebook.Unity;
using Firebase.Auth;
using Firebase;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;
using Firebase.Unity.Editor;
using Firebase.Database;
using System.Text;
using System.Text.RegularExpressions;

public class LoginManager : Singleton<LoginManager>
{
    DatabaseReference reference;
    bool userValidationFinished;
    public FirebaseAuth Auth => auth;
    private FirebaseAuth auth;
    public GameObject popUp;
    public UnityEvent OnLogin;
    public LogInScript login;
    public bool logged;
    string name;
    public Button playButton;

    // Start is called before the first frame update
    void Start()
    {
        Initialize();
        login = FindObjectOfType<LogInScript>();
        OnLogin.AddListener(() =>
        {
            ChangeScene();
        });
    }

    public void ReadyToStart()
    {
        playButton.interactable = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (logged)
        {
            Debug.Log("esto pasa");
            ChangeScene();
            logged = false;
        }
    }
    private void ProfilePhotoCallback(IGraphResult result)
    {
        if (String.IsNullOrEmpty(result.Error) && !result.Cancelled)
        { //if there isn't an error
            IDictionary data = result.ResultDictionary["data"] as IDictionary; //create a new data dictionary
            string photoURL = data["url"] as String; //add a URL field to the dictionary

            StartCoroutine(fetchProfilePic(photoURL)); //Call the coroutine to download the photo
        }
    }

    private IEnumerator fetchProfilePic(string url)
    {
        WWW www = new WWW(url); //use the photo url to get the photo from the web
        yield return www; //wait until it has downloaded
        DataManager.main.SetProfilePic(www.texture); //set your profilePic Image Component's sprite to the photo
    }
    public void TryToLogin()
    {
        var perms = new List<string>() { "public_profile", "email" };
        FB.LogInWithReadPermissions(perms, OnFacebookLoginResults);
    }

    private void OnFacebookLoginResults(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            var accesToken = AccessToken.CurrentAccessToken;
            Login(accesToken);
        }
        else
        {
            //SignInAction(obj: false);
        }
    }
    public async void Initialize()
    {
        var result = await FirebaseApp.CheckAndFixDependenciesAsync();
        if (result == DependencyStatus.Available)
        {
            auth = FirebaseAuth.DefaultInstance;
            FB.Init();
            //Initialize();
            FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://bingolegends-700c0.firebaseio.com/");
            // Get the root reference location of the database.
            reference = FirebaseDatabase.DefaultInstance.RootReference;
        }
        else
        {
            Debug.LogError("ALV");
        }
    }

    private void OnHideUnity(bool isUnityShown)
    {
        throw new NotImplementedException();
    }


    public void ChangeScene()
    {
        FB.API("/me/picture?redirect=false", HttpMethod.GET, ProfilePhotoCallback);
        DataManager.main.SetUserName(name);
        popUp.SetActive(true);
        popUp.GetComponentInChildren<Text>().text = "¡Bienvenid@ " + name + "!";
        Debug.Log("cambio");
        //SceneManager.LoadScene("ChooseLogged");
        //Debug.Log("cambio2");
    }
    public void Login(AccessToken accessToken)
    {
        Debug.Log("llegamos a loigin");
        var credential = FacebookAuthProvider.GetCredential(accessToken.TokenString);
        auth.SignInWithCredentialAsync(credential).ContinueWith(continuationAction: task =>
        {
            Debug.Log("corchetes");
            if (task.IsCanceled)
            {
                Debug.Log(message: "sing in cancelled");
                //Signin
            }
            else
            {
                Debug.Log("else");
                Firebase.Auth.FirebaseUser newUser = task.Result;
                Debug.LogFormat("User signed in successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);
                //SaveUser(newUser.UserId, newUser.DisplayName, newUser.Email);
                logged = true;
                name = newUser.DisplayName;
                //OnLogin.Invoke();
                //LogInScript.main.LogInButton();
            }
        });
        //    Firebase.Auth.Credential credential =
        //Firebase.Auth.FacebookAuthProvider.GetCredential(accessToken);
        //    auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
        //    {
        //        if (task.IsCanceled)
        //        {
        //            Debug.LogError("SignInWithCredentialAsync was canceled.");
        //            return;
        //        }
        //        if (task.IsFaulted)
        //        {
        //            Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
        //            return;
        //        }

        //        Firebase.Auth.FirebaseUser newUser = task.Result;
        //        Debug.LogFormat("User signed in successfully: {0} ({1})",
        //            newUser.DisplayName, newUser.UserId);
        //    });
    }

    private void SaveUser(string userId, string displayName, string email)
    {
        WriteNewFbUser(userId, displayName, email);
        Debug.Log("termino");
    }
    public void FindRegisteredUser(string userName, string password, Regist registClass)
    {
        CheckForRegisteredUser(userName, password, registClass);
    }

    public void CheckForCorrectPassword(string userName, string password, Regist regClass)
    {
        string foundedPassword = "";
        bool founded = false;
        string encodedstring = userName.Replace(".", "");
        string encodedstringPass = password.Replace(".", "");
        FirebaseDatabase.DefaultInstance
     .GetReference("users").Child(encodedstring).Child("password")
     .GetValueAsync().ContinueWith(task =>
     {
         if (task.IsFaulted)
         {
             // Handle the error...
         }
         else if (task.IsCompleted)
         {
             DataSnapshot snapshot = task.Result;
             Debug.Log(snapshot);
             foundedPassword = snapshot.Value.ToString(); 
             Debug.Log(foundedPassword + "  " + password);
             if (foundedPassword == encodedstringPass)
             {
                 Debug.Log("true");
                 regClass.OnPasswordsMatchesOver.Invoke(true);
             }
             else
             {
                 Debug.Log("false");
                 regClass.OnPasswordsMatchesOver.Invoke(false);
             }
         }
     });
    }
    public void GetNickname(string userName, string _email, Regist regClass)
    {
        string foundedPassword = "";
        bool founded = false;
        string encodedstring = _email.Replace(".", "");
        FirebaseDatabase.DefaultInstance
     .GetReference("users").Child(encodedstring).Child("username")
     .GetValueAsync().ContinueWith(task =>
     {
         if (task.IsFaulted)
         {
             Debug.Log("error en nickname");
         }
         else if (task.IsCompleted)
         {
             DataSnapshot snapshot = task.Result;
             Debug.Log(snapshot);
             regClass.OnNicknameFounded.Invoke(snapshot.Value.ToString());
             
         }
     });
    }
    IEnumerator FindLargest(int a, int b)
    {
        if (a > b)
            yield return new UserNameConsult(true);
        else
            yield return new UserNameConsult(false);
    }
    private void CheckForRegisteredUser(string userName, string password, Regist registClass)
    {
        bool founded = false;
        string encodedstring = userName.Replace(".", "");
        FirebaseDatabase.DefaultInstance
     .GetReference("users").Child(encodedstring)
     .GetValueAsync().ContinueWith(task =>
     {
         if (task.IsFaulted)
         {
             Debug.Log("falllaaaa maricon");
             
             // Handle the error...
         }
         else if (task.IsCompleted)
         {
             DataSnapshot snapshot = task.Result;
             Debug.Log(snapshot.Value);
             if (snapshot.Value != null)
             {
                 founded = true;
                 Debug.Log("encontrado");
                 registClass.OnUserSearchOver.Invoke(true);
                 CheckForCorrectPassword(userName, password,   registClass);
             }
             else
             {
                 founded = false;
                 Debug.Log("NO encontrado");
                 registClass.OnUserSearchOver.Invoke(false);
             }
         }
     });
    }
    public void WriteNewFbUser(string userId, string name, string email)
    {
        FbUser user = new FbUser(name, email);
        string json = JsonUtility.ToJson(user);

        reference.Child("users").Child(userId).SetRawJsonValueAsync(json);
        //SceneManager.LoadScene("ChooseLogged");
    }
    public void WriteNewUser(string password, string userName, string email)
    {
        string encodedstringuser = userName.Replace(".", "");
        string encodedstringemail = email.Replace(".", "");
        string encodedstringpassword = password .Replace(".", "");
        User user = new User(encodedstringuser, encodedstringemail, encodedstringpassword);
        string json = JsonUtility.ToJson(user);

        reference.Child("users").Child(encodedstringemail).SetRawJsonValueAsync(json);
        //SceneManager.LoadScene("ChooseLogged");
    }
}
public class FbUser
{
    public string username;
    public string email;
    public int score;

    public FbUser()
    {
    }

    public FbUser(string username, string email)
    {
        this.username = username;
        this.email = email;
        score = 0;
    }
}
public class User
{
    public string username;
    public string email;
    public int score;
    public string password;

    public User()
    {
    }

    public User(string username, string email, string password)
    {
        this.username = username;
        this.email = email;
        score = 0;
        this.password = password;
    }
}
public class CoroutineWithData<T>
{
    private IEnumerator _target;
    public T result;
    public Coroutine Coroutine { get; private set; }

    public CoroutineWithData(MonoBehaviour owner_, IEnumerator target_)
    {
        _target = target_;
        Coroutine = owner_.StartCoroutine(Run());
    }

    private IEnumerator Run()
    {
        while (_target.MoveNext())
        {
            result = (T)_target.Current;
            yield return result;
        }
    }
}

