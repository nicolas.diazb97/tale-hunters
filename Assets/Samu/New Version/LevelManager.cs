﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    public List<Level> levels;
    // Start is called before the first frame update
    void Start()
    {
        levels = GetComponentsInChildren<Level>(true).ToList();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
