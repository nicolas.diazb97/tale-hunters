﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostOff : MonoBehaviour
{
    public GameObject hostGameObject;
    public void TurnHostOff()
    {
        hostGameObject.SetActive(false);
    }
}
