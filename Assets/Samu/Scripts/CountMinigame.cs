﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CountMinigame : Singleton<CountMinigame>
{
    public float playerAnswer = 0;
    public Text answerText;
    public GameObject objToInstatiate;
    // Start is called before the first frame update
    void Start()
    {
        //InstantiateObj(46);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Add1()
    {
        if (playerAnswer < 99)
        {
            playerAnswer++;
        }
        else
        {
            playerAnswer = 0;
        }
        answerText.text = playerAnswer.ToString();
    }

    public void Minus1()
    {
        if (playerAnswer > 0)
        {
            playerAnswer--;
        }
        answerText.text = playerAnswer.ToString();
    }

    public void InstantiateObj(int _count)
    {
        List<MinigameItem> objs = GetComponentsInChildren<MinigameItem>(true).ToList();
        this.gameObject.SetActive(true);
        objs.ForEach(obj => obj.gameObject.SetActive(true));
        for (int i = 0; i < _count; i++)
        {
            Instantiate(objToInstatiate, transform.position, transform.rotation, this.transform);
        }
    }

    public void EndCountMiniGame()
    {
        Debug.Log("Entro al metodo EndCountMiniGame");
        this.gameObject.SetActive(false);
        List<MinigameItem> objs = GetComponentsInChildren<MinigameItem>(true).ToList();
        objs.ForEach(obj => obj.gameObject.SetActive(false));
    }
}
