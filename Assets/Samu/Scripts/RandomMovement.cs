﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMovement : MonoBehaviour
{
    RectTransform limitGameObject;
    RectTransform currentObj;
    public float speed;
    float horizontalSpeedMultiplier = 1f;
    float verticalSpeedMultiplier = -1f;

    void Start()
    {
        currentObj = gameObject.GetComponent<RectTransform>();
        limitGameObject = GameObject.Find("Limit").GetComponent<RectTransform>();
        horizontalSpeedMultiplier = Random.Range(-1, 1);
        verticalSpeedMultiplier = Random.Range(-1, 1);
        speed = Random.Range(1f, 10f);
    }

    // Update is called once per frame
    void Update()
    {
        MoveRandomly();
    }

    void MoveRandomly()
    {
        if(horizontalSpeedMultiplier == 0f && verticalSpeedMultiplier == 0f)
        {
            horizontalSpeedMultiplier = Random.Range(-1, 1);
            verticalSpeedMultiplier = Random.Range(-1, 1);
        }

        transform.Translate(horizontalSpeedMultiplier * speed, verticalSpeedMultiplier * speed, 0f);

        if (currentObj.position.y > limitGameObject.rect.height)
        {
            verticalSpeedMultiplier = -1f;
        }

        if (currentObj.position.y < 0f)
        {
            verticalSpeedMultiplier = 1f;
        }

        if (currentObj.position.x < 0f)
        {
            horizontalSpeedMultiplier = 1f;
        }

        if (currentObj.position.x > limitGameObject.rect.width)
        {
            horizontalSpeedMultiplier = -1f;
        }
    }
}
