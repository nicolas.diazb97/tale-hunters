﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerSam : MonoBehaviour
{
    public Text timer;
    public float currCountdownValue = 15;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartCountDown());
    }

    // Update is called once per frame
    void Update()
    {
        timer.text = currCountdownValue.ToString();

    }

    public IEnumerator StartCountDown()
    {
        while (currCountdownValue > 0)
        {
            Debug.Log("Countdown: " + currCountdownValue);
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
        }
    }
}
