﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideMinigame : MonoBehaviour
{
    public float number1;
    public float number2;
    public float number3;
    public float answer;

    public float firstSignValue;
    public float secondSignValue;

    public Text number1Text;
    public Text number2Text;
    public Text number3Text;
    public Text firstSign; 
    public Text secondSign;
    public Text realAnswer;

    void Start()
    {
        RandomValues();
        SignAssignation();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RandomValues()
    {
        number1 = Random.Range(1, 30);
        number2 = Random.Range(1, 30);
        number3 = Random.Range(1, 30);

        number1Text.text = number1.ToString();
        number2Text.text = number2.ToString();
        number3Text.text = number3.ToString();

        firstSignValue = Random.Range(1, 4);
        secondSignValue = Random.Range(1, 4);      

        Operations();
    }

    public void Operations()
    {
        if (firstSignValue == 1 || firstSignValue == 3)
        {        
            if (secondSignValue == 1 || secondSignValue == 3)
            {
                answer = number1 + number2 + number3;
                realAnswer.text = answer.ToString();
            }

            if (secondSignValue == 2 || secondSignValue == 4)
            {
                answer = number1 + number2 - number3;
                realAnswer.text = answer.ToString();
            }
        }

        if (firstSignValue == 2 || firstSignValue == 4)
        {
            if (secondSignValue == 1 || secondSignValue == 3)
            {
                answer = number1 - number2 + number3;
                realAnswer.text = answer.ToString();
            }

            if (secondSignValue == 2 || secondSignValue == 4)
            {
                answer = number1 - number2 - number3;
                realAnswer.text = answer.ToString();
            }
        }
    }

    public void SignAssignation()
    {
        if (firstSignValue == 1)
        {
            firstSign.text = "+";
        }

        if (firstSignValue == 2)
        {
            firstSign.text = "-";
        }

        if (secondSignValue == 1)
        {
            secondSign.text = "+";
        }

        if (secondSignValue == 2)
        {
            secondSign.text = "-";
        }
    }
}
