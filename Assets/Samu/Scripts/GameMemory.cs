﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameMemory
{
    [SerializeField]
    public int position1;
    [SerializeField]
    public int position2;
    [SerializeField]
    public int position3;
}
