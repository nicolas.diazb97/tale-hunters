﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MemotyMinigame : MonoBehaviour
{

    [SerializeField]
    private Transform firstPosition;

    private Vector2 initialPosition;

    private float deltaX, deltaY;

    public static bool locked;

    public Text example;
    void Start()
    {
        initialPosition = transform.position;
    }

    private void Update()
    {
        if(Input.touchCount > 0 && !locked)
        {
            Touch touch = Input.GetTouch(0);
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if(GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos))
                    {
                        deltaX = touchPos.x - transform.position.x;
                        deltaY = touchPos.y - transform.position.y;
                    }
                    example.text = "Toco";
                    break;

                case TouchPhase.Moved:
                    if(GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos))                    
                        transform.position = new Vector2(touchPos.x - deltaX, touchPos.y - deltaY);                   
                    example.text = "movio";
                    break;

                case TouchPhase.Ended:
                    if(Mathf.Abs(transform.position.x - firstPosition.position.x) <= 0.5f &&
                       Mathf.Abs(transform.position.y - firstPosition.position.y) <= 0.5f)
                    {
                        transform.position = new Vector2(firstPosition.position.x, firstPosition.position.y);
                        locked = true;
                    }
                    else
                    {
                        transform.position = new Vector2(initialPosition.x, initialPosition.y);
                    }
                    example.text = "Termino";
                    break;
            }
        }
    }
}
