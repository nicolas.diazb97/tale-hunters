﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastOpacity : MonoBehaviour
{
    public Transform Player;
    public GameObject LastTower;
    public GameObject[] towers;
    Texture ActualTexture;
    bool NewTower = true, animOpacityUp = false, animOpacityDown = false, OnAnimationEnded = false;
    // Start is called before the first frame update
    void Start()
    {
        towers = GameObject.FindGameObjectsWithTag("tower");
        //for (int i = 0; i < towers.Length; i++)
        //{
        //    Material m = towers[i].GetComponent<MeshRenderer>().materials[0];
        //    m.SetFloat("_Mode", 0);
        //    m.SetInt("_ZWrite", 1);
        //    m.renderQueue = 3000;
        //    towers[i].GetComponent<MeshRenderer>().materials[0].SetColor("_Color", new Color32(255, 255, 255, 255));
        //}
    }

    // Update is called once per frame
    void Update()
    {
        /*

        int layerMask = 1 << 8;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(Player.transform.position, Player.transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(Player.transform.position, Player.transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            //Debug.Log(hit.collider.tag);
            if (LastTower != hit.collider.gameObject)
            {
                //LastTower.GetComponent<MeshRenderer>().materials[0] = opaque;
                //LastTower.GetComponent<MeshRenderer>().materials[0].SetTexture("_MainTex", ActualTexture);

                LastTower.GetComponent<tower>().ChangeMaterialToOpaque();


                //Material m = LastTower.GetComponent<MeshRenderer>().materials[0];
                //m.SetFloat("_Mode", 0);
                //m.SetInt("_ZWrite", 1);
                //m.renderQueue = 3000;
                //LastTower.GetComponent<MeshRenderer>().materials[0].SetColor("_Color", new Color32(255, 255, 255, 255));
            }
            if (hit.collider.tag == "tower")
            {
                Debug.Log("positivo para tower");
                if (LastTower == null)
                {
                    LastTower = hit.collider.gameObject;
                }
                //hit.collider.gameObject.GetComponent<MeshRenderer>().materials[0].SetFloat("_Mode", 3f);
                //ActualTexture = hit.collider.gameObject.GetComponent<MeshRenderer>().materials[0].GetTexture("_MainTex");
                //hit.collider.gameObject.GetComponent<MeshRenderer>().materials[0] = Transparent;
                //hit.collider.gameObject.GetComponent<MeshRenderer>().materials[0].SetTexture("_MainTex", ActualTexture);


                //Material m = hit.collider.gameObject.GetComponent<MeshRenderer>().materials[0];
                //m.SetFloat("_Mode", 3);
                //m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                //m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                //m.SetInt("_ZWrite", 0);
                //m.SetColor("_Color", new Color32(255, 255, 255, 100));
                //m.renderQueue = 3000;
                //hit.collider.gameObject.GetComponent<MeshRenderer>().UpdateGIMaterials();
                LastTower = hit.collider.gameObject;
                LastTower.GetComponent<tower>().ChangeMaterialToTransparent();
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            Debug.Log("Did not Hit");
        }
        */
    }
}
