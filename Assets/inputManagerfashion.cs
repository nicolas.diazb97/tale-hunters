﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inputManagerfashion : Singleton<inputManagerfashion>
{
    public Transform currPivotCloth;
    float rightFirstTouchRight;
    float leftFirstTouch;
    float leftCurrTouch;
    float rightCurrTouch;
    float lastRightTouch = 0;
    float lastLeftTouch = 0;
    bool isFirstTapRight = true;
    bool isFirstTapLeft = true;
    public bool tutorial;
    public float currMovement { private set; get; }
    public float rotationSpeed;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            //Debug.Log("derechaaaaaa" + rightCurrTouch);
            //Debug.Log("izquierdaaaa" + leftCurrTouch);
            Touch myTouch = Input.GetTouch(0);

            Touch[] myTouches = Input.touches;
            for (int i = 0; i < Input.touchCount; i++)
            {
                //Do something with the touches
                //Debug.Log(myTouches[i].position);
                if (myTouches[i].position.x > Screen.width / 2)
                {
                    //Debug.Log("right" + myTouches[i].position.y);
                    if (isFirstTapRight)
                    {
                        rightFirstTouchRight = myTouches[i].position.y;
                        lastRightTouch = myTouches[i].position.y;
                    }
                    isFirstTapRight = false;
                    rightCurrTouch = myTouches[i].position.y - lastRightTouch;
                    //sewMachine.transform.Rotate(sewMachine.transform.rotation.x, sewMachine.transform.rotation.y, -20*Time.deltaTime, Space.Self);
                }
                else
                {
                    //Debug.Log("left" + myTouches[i].position.y);
                    if (isFirstTapLeft)
                    {
                        leftFirstTouch = myTouches[i].position.y;
                        lastLeftTouch = myTouches[i].position.y;
                    }
                    isFirstTapLeft = false;
                    leftCurrTouch = myTouches[i].position.y - lastLeftTouch;
                    //sewMachine.transform.Rotate(sewMachine.transform.rotation.x, sewMachine.transform.rotation.y, 20 * Time.deltaTime, Space.Self);
                }
            }
            currMovement = (rightCurrTouch + -leftCurrTouch) / Screen.height;
        }
        else
        {
            isFirstTapRight = true;
            isFirstTapLeft = true;
        }
        if (Input.touchCount > 1)
        {
            currPivotCloth.transform.rotation = Quaternion.Euler(currPivotCloth.transform.rotation.x, currPivotCloth.transform.rotation.y, currPivotCloth.transform.rotation.z + currMovement * rotationSpeed);
            if (tutorial)
            {
                if (currMovement > 0)
                {
                    //TutorialManager.main.FirstStepDone();
                }
                if (currMovement < 0)
                {
                    //TutorialManager.main.SecondStepDone();
                }
            }
        }
    }


}
