﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmojiManager : MonoBehaviour
{
    public GameObject[] emojis;
    public Sprite[] emojisSprites;
    public GameObject emojiImg;
    // Start is called before the first frame update
    public void NewEmoji(Emoji _emoji)
    {
        Player player = PlayerManager.main.GetPlayerById(_emoji.playerId);
        emojiImg = player.GetComponentInChildren<EmojiImage>(true).gameObject;
        //emojiImg.SetActive(true);
        GameObject tmpEmoji = Instantiate(emojis[_emoji.emojiId], player.transform.position, player.transform.rotation, player.transform);
        Destroy(tmpEmoji, 3f);
        //emojiImg.GetComponent<Image>().sprite = emojisSprites[_emoji.emojiId];

        //StartCoroutine(WaitToQuitEmoji(player.GetComponentInChildren<EmojiImage>().GetComponent<Image>()));
    }
    public IEnumerator WaitToQuitEmoji(Image sprite)
    {
        yield return new WaitForSecondsRealtime(3f);
        emojiImg.SetActive(false);
    }
}
