﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginWithEmail : MonoBehaviour
{
    public Text error;
    public InputField email;
    public InputField password;
    public string nicknameFromDB;
    public GameObject popUp;
    public Texture2D defaultPic;
    bool userFounded;
    bool passwordMatches;
    bool loginCompleted;
    private bool userNotFound;

    public void TryToLogin()
    {
        Regist regCallback = new Regist();
        if (CheckForEmptySpaces())
        {
            Debug.Log("esto paswa");
            Debug.Log("esto paswa");
            regCallback.OnUserSearchOver.AddListener((bool t) =>
            {
                UserSearchOver(t);
            });
            regCallback.OnPasswordsMatchesOver.AddListener((bool t) =>
            {
                UserPasswordOver(t);
            });
            LoginManager.main.FindRegisteredUser(email.text, password.text, regCallback);
        }
    }

    private void UserPasswordOver(bool userExists)
    {
        if (userExists)
        {
            Debug.Log("busqueda password finalizada en true");
            passwordMatches = true;
        }
        else
        {
            Debug.Log("busqueda password finalizada en false");
            userNotFound = true;
        }
    }

    private void Update()
    {
            Debug.Log("userf: "+userFounded+" passmatch: "+passwordMatches);
        if (userFounded && passwordMatches)
        {
            Debug.Log("buscando nick");
            Regist regCallback = new Regist();
            regCallback.OnNicknameFounded.AddListener((string s) =>
            {
                NicknameFounded(s);
            });
            userFounded = false;
            passwordMatches = false;
            LoginManager.main.GetNickname("",email.text, regCallback);
        }
        if (loginCompleted)
        {
            ChangeScene();
            loginCompleted = false;
        }
        if (userNotFound)
        {
            error.text = "Contraseña y/o correo incorrecto";
            userNotFound = false;
        }
    }
    public void ChangeScene()
    {
        DataManager.main.SetProfilePic(defaultPic);
        DataManager.main.SetUserName(nicknameFromDB);
        popUp.SetActive(true);
        popUp.GetComponentInChildren<Text>().text = "¡Bienvenid@ " + nicknameFromDB + "!";
        Debug.Log("cambio");
        //SceneManager.LoadScene("ChooseLogged");
        //Debug.Log("cambio2");
    }

    private void NicknameFounded(string nickname)
    {
        nicknameFromDB = nickname;
        Debug.Log("lo logro, el maldito hijo de perra lo logro");
        loginCompleted = true;
    }

    public bool CheckForEmptySpaces()
    {
        if (email.text != "" && password.text != "")
            return true;
        else
            return false;
    }
    private void UserSearchOver(bool userExists)
    {
        Debug.Log("busqueda finalizada");
        if (!userExists)
        {
            userNotFound = true;
        }
        else
        {
            userFounded = true;
        }
    }
}
