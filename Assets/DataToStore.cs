﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataToStore
{ 
    [SerializeField]
    public string nameId;
    [SerializeField]
    public string image;
    [SerializeField]
    public string currId;
    [SerializeField]
    public bool isPlayingAnActiveGame;
    public DataToStore(string _image, string _name, string _currid,bool _isPlayingAnActiveGame)
    {
        image = _image;
        nameId = _name;
        currId = _currid;
        isPlayingAnActiveGame = _isPlayingAnActiveGame;
    }
}
