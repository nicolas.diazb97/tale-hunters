﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogInScript : MonoBehaviour
{
    public void BackButton()
    {
        SceneManager.LoadScene("UserMenu");
    }

    public void LogInButton()
    {
        Debug.Log("cammarEscena");
        SceneManager.LoadScene("ChooseLogged");
        Debug.Log("cammarEscena2");
    }

    public void RegisterButton()
    {
        SceneManager.LoadScene("Register");
    }
}
