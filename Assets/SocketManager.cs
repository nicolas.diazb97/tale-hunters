﻿using UnityEngine;
using System.Collections;
using UnitySocketIO;
using UnitySocketIO.Events;
using System.Text.RegularExpressions;
using System;
using System.Linq;
using UnityEngine.UI;

public class SocketManager : MonoBehaviour
{
    PlayerManager playerManager;
    UiManager uiManager;
    public SocketIOController io;
    EmojiManager emojiManager;
    public int currLevel = 0;

    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
        emojiManager = FindObjectOfType<EmojiManager>();
        uiManager = FindObjectOfType<UiManager>();
        io.On("connect", (SocketIOEvent e) =>
        {
            Debug.Log("SocketIO connected" + io.SocketID);
            //TestObject t = new TestObject();
            //t.test = 123;
            //t.test2 = "test1";

            //io.Emit("test-event2", JsonUtility.ToJson(t));

            //TestObject t2 = new TestObject();
            //t2.test = 1234;
            //t2.test2 = "test2";

            //io.Emit("test-event3", JsonUtility.ToJson(t2), (string data) => {
            //    Debug.Log(data);
            //});

        });
        io.Connect();
        io.On("hola", Received);
        io.On("emojiReceived", EmojiReceived);
        io.On("updateLobby", IncomingPlayer);
        io.On("a-player-win", PlayerLose);
        io.On("a-player-checking-failed", APlayerFailed);
        io.On("ballots-check-Successful", PlayerWin);
        io.On("noGameFound", LobbyGameNotFound);
        io.On("playerJoinGame", PlayerJoinGame);
        io.On("newBallot", IncomingNumber);
        io.On("gameStarted", StartGameForAll);
        io.On("conn", CheckForRefreshedConn);
        io.On("refreshBallots", RefreshNumbers);
        io.On("updatePlayerId", UpdatePlayerId);
        io.On("playerTurn", PlayerTurn);
        io.On("moveToSection", MoveToSection);
        io.On("moveToSpecialSection", MoveToSpecialSection);
        io.On("minigamedice", MiniGameDiceCallback);
        io.On("returnToBoard", returnToBoard);
        io.On("init-game-count", InitGameCount);
        io.On("position-game-count", PositionGameCount);
        io.On("timer-game-count", EndGameCount);
        io.On("init-game-memory", InitGameMemoryMinigame);
        io.On("init-game-hide-away", InitGameHideaway);
        io.On("timer-game-memory", EndGameMemory);
        io.On("position-game-memory", PositionGameMemory);
        io.On("timer-game-hideaway", ResultGameHideaway);
        io.On("response-game-hideaway", ResponseGameHideaway);
        io.On("position-game-hideaway", PositionGameHideaway);
        io.On("timer-round-hideaway", NewRoundGameHideaway);
        io.On("init-game-card", InitGameCard);
        io.On("game-is-over", GameOver);
        io.On("final-results", FinalResults);
        io.On("activate-board", ActivateCurrLevel);
        io.On("timer-game-card", ResultGameCard);
        io.On("response-game-card", ResponseGameCard);
        io.On("position-game-card", PositionGameCard);
        io.On("init-game-card-count", InitGameCardCount);
        io.On("timer-game-card-count", TimerGameCardCount);
        io.On("position-game-card-count", PositionGameCardCount);
        io.On("result-game-card-count", ResultGameCardCount);
        io.On("player-eliminated", PlayerEliminated);
    }
    public void GameOver(SocketIOEvent obj)
    {
        Debug.LogError("AQUI PUTOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
        io.Emit("final-pos", JsonUtility.ToJson(playerManager.GetPlayerById(io.SocketID).refToPlayer.currSectionId, true));
    }
    public void ClaimNewSpecialSection(int _section)
    {
        int gamePin = int.Parse(DataManager.main.GetGamePin());
        string nameId = DataManager.main.GetUserName();
        PlayerJoinData dataToSend = new PlayerJoinData(nameId, gamePin, _section.ToString(), currLevel);
        io.Emit("claim-special-point", JsonUtility.ToJson(dataToSend, true));
        Debug.LogError("claiming for special section");
    }
    public void FinalResults(SocketIOEvent obj)
    {
        resultGameCount[] result = JsonHelper.FromJson<resultGameCount>(fixJson(obj.data + "]"));
        Debug.LogError("AQUI WE LOLLLLLLLLLLLLLLLLLLLLLLLL" + result[0].position);
        for (int i = 0; i < result.Length; i++)
        {
            MinigamePosition currPosition = new MinigamePosition(new MinigameCountInstance(), result[i].position);
            PlayerManager.main.GetPlayerById(result[i].playerId).SetPostion(currPosition);

        }
        //Debug.Log("Prueba de la posicion: " + result.position);
        //CountMinigame.main.EndCountMiniGame();
        PositionsManager.main.ShowLastPosition(new MinigameCountInstance());
    }

    private void returnToBoard(SocketIOEvent obj)
    {
        uiManager.ToBoard();
    }

    private void MiniGameDiceCallback(SocketIOEvent obj)
    {
        MinigameController.main.RollDice();
    }

    private void MoveToSection(SocketIOEvent obj)
    {
        IncomingDataPlayer newPlayer = JsonUtility.FromJson<IncomingDataPlayer>(obj.data.Trim('['));
        Debug.Log(newPlayer.playerId + " lanza el dado");
        int finalSection = newPlayer.posOnBoard + newPlayer.diceNumber;
        uiManager.SetDice(newPlayer.diceNumber.ToString());
        UiManager.main.uiTurn.EndTurn();
        DiceManager.main.RollDice(PlayerManager.main.GetPlayerById(newPlayer.playerId), finalSection);
        Debug.Log("casillas a moverse: " + obj.data.ToString().Trim('"'));
        Debug.LogError("movetosection");
        UiManager.main.DiceAnim("Fall" + newPlayer.diceNumber.ToString());
        playerManager.PlayAnimation(newPlayer.playerId, "Walk");
        ClaimNewTurn();
        //CreateGameCardCount();
    }
    private void MoveToSpecialSection(SocketIOEvent obj)
    {
        Debug.LogError("moving to special");
        IncomingDataPlayer newPlayer = JsonUtility.FromJson<IncomingDataPlayer>(obj.data.Trim('['));
        Debug.Log(newPlayer.playerId + " lanza el dado");
        int finalSection = newPlayer.diceNumber;
        uiManager.SetDice(newPlayer.diceNumber.ToString());
        UiManager.main.uiTurn.EndTurn();
        DiceManager.main.MoveTo(PlayerManager.main.GetPlayerById(newPlayer.playerId), finalSection);
        Debug.Log("casillas a moverse: " + obj.data.ToString().Trim('"'));
        Debug.LogError("movetosection");
        UiManager.main.DiceAnim("Fall" + newPlayer.diceNumber.ToString());
        playerManager.PlayAnimation(newPlayer.playerId, "BeReady");
        playerManager.PlayAnimation(newPlayer.playerId, "Walk");
        ClaimNewTurn();
        //CreateGameCardCount();
    }

    public void CreateGameCount()
    {
        io.Emit("create-game-count");
    }

    public void CreateGameMemory()
    {
        io.Emit("create-game-memory");
    }

    public void CreateGameHideaway()
    {
        io.Emit("create-game-hideaway");
    }

    public void CreateGameCard()
    {
        io.Emit("create-game-card");
    }

    public void CreateGameCardCount()
    {
        io.Emit("create-game-card-count");
    }

    public void InitGameCount(SocketIOEvent obj)
    {
        Debug.Log("Prueba de la respuesta: " + obj.data.ToString().Trim('"'));
        string number = obj.data.ToString();
        CountMinigame.main.InstantiateObj(int.Parse(number.ToString()));
    }

    public void ActivateCurrLevel(SocketIOEvent obj)
    {
        Debug.Log("Activating response: ");
        Debug.Log("Activating response: " + obj.data.ToString());
        string number = obj.data.ToString();
        PlayerManager.main.InitReferences(int.Parse(obj.data.ToString()));
        LevelManager.main.levels.Where(lvl => lvl.id.ToString() == obj.data.ToString()).ToList()[0].gameObject.SetActive(true);
    }

    public void InitGameCard(SocketIOEvent obj)
    {
        MiniGameCard[] data = JsonHelper.FromJson<MiniGameCard>(fixJson(obj.data + "]"));
        int[] array = new int[6];
        for (int i = 0; i < 6; i++)
        {
            array[i] = data[i].number;
        }
        Debug.Log("Prueba de la respuesta: " + obj.data.ToString().Trim('"'));
    }

    public void InitGameCardCount(SocketIOEvent obj)
    {
        MiniGameCard[] data = JsonHelper.FromJson<MiniGameCard>(fixJson(obj.data + "]"));
        int[] array = new int[6];
        for(int i = 0; i < 6; i++)
        {
            array[i] = data[i].number;
        }
        Debug.Log("Respuesta cuenta cartas: " + obj.data.ToString().Trim('"'));
    }

    public void InitGameMemoryMinigame(SocketIOEvent obj)
    {
        Debug.Log("Prueba de la respuesta: " + obj.data.ToString().Trim('"'));
        GameMemory[] response = JsonHelper.FromJson<GameMemory>(fixJson(obj.data + "]"));

        int[,] matriz = new int[4, 3];
        Debug.Log("response " + response[0].position1);
        matriz[0, 0] = response[0].position1;
        matriz[0, 1] = response[0].position2;
        matriz[0, 2] = response[0].position3;
        matriz[1, 0] = response[1].position1;
        matriz[1, 1] = response[1].position2;
        matriz[1, 2] = response[1].position3;
        matriz[2, 0] = response[2].position1;
        matriz[2, 1] = response[2].position2;
        matriz[2, 2] = response[2].position3;
        matriz[3, 0] = response[3].position1;
        matriz[3, 1] = response[3].position2;
        matriz[3, 2] = response[3].position3;

        Debug.Log("matriz " + matriz[0, 0]);
        MemoRyMinigame.main.InstantiateObj(matriz);
    }

    public void TimerGameCardCount(SocketIOEvent obj)
    {
        string number = obj.data.ToString();
        Debug.Log("Segundos cuenta cartas: " + number);

        if (int.Parse(number.ToString()) == 0)
        {
            prueba result = new prueba(20);
            io.Emit("result-game-card-count", JsonUtility.ToJson(result, true));
        }
    } 

    public void PositionGameCardCount(SocketIOEvent obj)
    {
        Debug.Log("Posiciones cuenta cartas " + obj.data.ToString().Trim('"'));
    }

    public void ResultGameCardCount(SocketIOEvent obj)
    {
        Debug.Log("Resultado cuenta cartas " + obj.data.ToString().Trim('"'));
    }

    public void PlayerEliminated(SocketIOEvent obj)
    {
        Debug.Log("Jugador eliminado cuenta cartas " + obj.data.ToString().Trim('"'));
    }

    public void EndGameMemory(SocketIOEvent obj) {
        string number = obj.data.ToString();
        Debug.Log("Segundos: "+number);
        if (int.Parse(number.ToString()) == 0)
        {
            //Debug.Log("Posicion de la matriz "+result[0, 0]);
            ResultGameMemory[] data = new ResultGameMemory[4];

            data[0] = new ResultGameMemory();
            data[0].position1 = "1";
            data[0].position2 = "2";
            data[0].position3 = "3";

            data[1] = new ResultGameMemory();
            data[1].position1 = "4";
            data[1].position2 = "5";
            data[1].position3 = "6";

            data[2] = new ResultGameMemory();
            data[2].position1 = "7";
            data[2].position2 = "8";
            data[2].position3 = "9";

            data[3] = new ResultGameMemory();
            data[3].position1 = "10";
            data[3].position2 = "11";
            data[3].position3 = "12";


            io.Emit("result-game-memory", JsonHelper.ToJson(data, true));
        }
    }

    public void PositionGameMemory(SocketIOEvent obj) {
        PositionGameMemory[] positions = JsonHelper.FromJson<PositionGameMemory>(fixJson(obj.data + "]"));
        Debug.Log("Posición "+ positions[0].position);
    }

    public void EndGameCount(SocketIOEvent obj)
    {
        string number = obj.data.ToString();
        if (int.Parse(number.ToString()) == 0)
        {
            prueba result = new prueba(int.Parse(FindObjectOfType<MiniGameCountAnswer>().GetComponent<Text>().text));
            io.Emit("result-game-Count", JsonUtility.ToJson(result, true));
        }
        Debug.Log("Prueba de fin del juego: " + obj.data.ToString().Trim('"'));
    }

    public void InitGameHideaway(SocketIOEvent obj)
    {
        MiniGameHideaway[] response = JsonHelper.FromJson<MiniGameHideaway>(fixJson(obj.data + "]"));
        Debug.Log("Ver operacion" + response[0].castle);
        Debug.Log("Respuesta escondite: " + obj.data.ToString().Trim('"'));
        ManagerMiniGameHide.main.InstantiateObj(response[0].operation.ToString(), int.Parse(response[0].tower.ToString()), int.Parse(response[0].castle.ToString()), int.Parse(response[0].pit.ToString()), int.Parse(response[0].rock.ToString()));
    }

    public void ResultGameHideaway(SocketIOEvent obj)
    {
        string number = obj.data.ToString();
        Debug.Log("Segundos de escondite"+ number);
        if(int.Parse(number.ToString()) == 0)
        {
            prueba result = new prueba(20);
            io.Emit("result-game-hideaway", JsonUtility.ToJson(result, true));
        }
    }

    public void ResultGameCard(SocketIOEvent obj)
    {
        string number = obj.data.ToString();
        Debug.Log("Segundos de cartas" + number);
        if (int.Parse(number.ToString()) == 0)
        {
            prueba result = new prueba(20);
            io.Emit("result-game-card", JsonUtility.ToJson(result, true));
        }
    }

    public void ResponseGameCard(SocketIOEvent obj)
    {
        string response = obj.data.ToString();
        Debug.Log("Respuesta de la ronda cartas: " + response);
    }

    public void PositionGameCard(SocketIOEvent obj)
    {
        string response = obj.data.ToString();
        Debug.Log("Posiciones cartas: " + response);
    }

    public void NewRoundGameHideaway(SocketIOEvent obj)
    {
        string number = obj.data.ToString();
        Debug.Log("Segundos de escondite" + number);
        if (int.Parse(number.ToString()) == 0)
        {
            prueba result = new prueba(20);
            io.Emit("new-round-game-hideaway");
        }
    }

    public void ResponseGameHideaway(SocketIOEvent obj)
    {
        string response = obj.data.ToString();
        Debug.Log("Respuesta de la ronda: "+ response);
    }

    public void PositionGameHideaway(SocketIOEvent obj)
    {
        string response = obj.data.ToString();
        Debug.Log("Posiciones escondite: " + response);
    }

    public void PositionGameCount(SocketIOEvent obj)
    {
        EndOfMinigames();
        resultGameCount[] result = JsonHelper.FromJson<resultGameCount>(fixJson(obj.data + "]"));
        Debug.LogError("aqui es we " + result[0].position);
        for (int i = 0; i < result.Length; i++)
        {
            MinigamePosition currPosition = new MinigamePosition(new MinigameCountInstance(), result[i].position);
            PlayerManager.main.GetPlayerById(result[i].playerId).SetPostion(currPosition);

        }
        //Debug.Log("Prueba de la posicion: " + result.position);
        CountMinigame.main.EndCountMiniGame();
        PositionsManager.main.ShowPosition(new MinigameCountInstance());
    }

    public void EndOfMinigames()
    {
        io.Emit("endminigame", JsonUtility.ToJson("0", true));
        io.Emit("newTurn", JsonUtility.ToJson("0", true));

    }

    public void EndOfTheGame()
    {
        io.Emit("player-reached-end-of-the-Game", JsonUtility.ToJson("0", true));
    }
    public void ClearInterval()
    {
        io.Emit("clearInterval", JsonUtility.ToJson("0", true));
    }
    public void ClearIntervalFull()
    {
        io.Emit("clear", JsonUtility.ToJson("0", true)); 
    }
    public void RollMiniGameDice()
    {
        io.Emit("minigameDice", JsonUtility.ToJson("0", true)); 

    }
    public void ClaimNewTurn()
    {
        int gamePin = int.Parse(DataManager.main.GetGamePin());
        string nameId = DataManager.main.GetUserName();
        int lvl = 3;
        io.Emit("test", "dgsgds");
        PlayerJoinData dataToSend = new PlayerJoinData(nameId, gamePin, "1", currLevel);
        io.Emit("newTurn", JsonUtility.ToJson(dataToSend, true));
    }

    public void ClaimNewTurn2()
    {
        io.Emit("endminigame", JsonUtility.ToJson("0", true));
        io.Emit("newTurn", JsonUtility.ToJson("0", true));
    }
    private void PlayerTurn(SocketIOEvent obj)
    {
        DiceManager.main.ActivateCamera(PlayerManager.main.GetPlayerById(obj.data.ToString().Trim('"')));
        Debug.Log(obj.data.ToString().Trim('"'));
        uiManager.SetTurn(obj.data.ToString().Trim('"'), io.SocketID);
        playerManager.PlayAnimation(obj.data.ToString().Trim('"'), "BeReady");
    }

    public void SendProfilePic()
    {
        string stringData = Convert.ToBase64String(DataManager.main.GetProfilePic().EncodeToJPG()); //1

        io.Emit("updateProfPic", JsonUtility.ToJson(stringData, true));
    }
    private void UpdatePlayerId(SocketIOEvent obj)
    {
        string[] sockets = JsonHelper.FromJson<string>(fixJson(obj.data + "]"));
        playerManager.UpdatePlayerId(playerManager.GetPlayerById(sockets[0]), sockets[1]);
    }

    private void RefreshNumbers(SocketIOEvent obj)
    {
        if (uiManager.isPlaying)
        {
            string[] numbers = JsonHelper.FromJson<string>(fixJson(obj.data + "]"));
            uiManager.ClearLogBingoNumbers();
            for (int i = 0; i < numbers.Length; i++)
            {
                if (int.Parse(numbers[i]) == 1)
                {
                    Debug.Log("refreshed Number: " + i);
                    uiManager.SetLogBingoNumbers(i.ToString());
                }
            }
        }
    }

    private void CheckForRefreshedConn(SocketIOEvent obj)
    {
        Debug.Log("connected to server");
        DataManager.main.NewConnection();
        if (DataManager.main.CheckForRefreshedConnections())
        {
            UpdateSocketId(DataManager.main.GetSocketId());
        }
        DataManager.main.SetSocketId(io.SocketID);
        DataManager.main.SaveCurrLocalData(true);
    }
    public void CallBackPlayerIsOnGame()
    {
        Debug.Log("emitting callback");
        io.Emit("playerEnterGame", JsonUtility.ToJson("", true));
    }
    private void UpdateSocketId(string oldSocketId)
    {
        RefreshData dataToSend = new RefreshData(oldSocketId);
        io.Emit("updatePlayerSocketId", JsonUtility.ToJson(dataToSend, true));
        DataManager.main.Log();
    }

    private void EmojiReceived(SocketIOEvent obj)
    {
        Emoji nEmoji = JsonUtility.FromJson<Emoji>(obj.data.Trim('['));
        emojiManager.NewEmoji(nEmoji);
        Debug.Log(nEmoji.emojiId);
    }

    private void APlayerFailed(SocketIOEvent obj)
    {
        uiManager.SetChatLog(obj.data + " cantó bingo erroneo");
    }

    private void PlayerWin(SocketIOEvent obj)
    {
        uiManager.SetWinner("Has ganado, " + obj.data);
    }

    private void PlayerLose(SocketIOEvent obj)
    {
        uiManager.SetWinner(obj.data + " ha ganado");
    }

    private void StartGameForAll(SocketIOEvent obj)
    {
        Debug.LogError("de aqui no pasa 0");
        IncomingPlayer(obj);
        Debug.LogError("de aqui no pasa");
        Screen.orientation = ScreenOrientation.Portrait;
        uiManager.ActivateBoard();
        Debug.LogError("de aqui no pasa 2");
        playerManager.GetPlayerById(io.SocketID).currPlayer = true;
        Debug.LogError("de aqui no pasa 2");
    }

    private void IncomingNumber(SocketIOEvent obj)
    {
        Debug.Log(obj.data);
        uiManager.SetLogBingoNumbers(obj.data.ToString());
        if (!uiManager.bingoContainer.activeInHierarchy)
        {
            uiManager.ActivateBoard();
        }
    }

    private void PlayerJoinGame(SocketIOEvent obj)
    {
        uiManager.PlayerFoundGame();
    }

    private void LobbyGameNotFound(SocketIOEvent obj)
    {
        uiManager.SetLog("juego no encontrado");
        throw new NotImplementedException();
    }

    public void CreateGame()
    {
        uiManager.DeactivateSingleClickButton("CreateGameButton");
        PlayerManager.main.InitReferences(currLevel);
        io.Emit("host-join-game", "9874");
    }
    public void JoinGame()
    {
        int gamePin = int.Parse(uiManager.GetInputGamePin());
        DataManager.main.SaveGamePin(gamePin.ToString());
        PlayerManager.main.InitReferences(currLevel);
        string nameId = DataManager.main.GetUserName();
        io.Emit("test", "dgsgds");
        string stringPic = Convert.ToBase64String(DataManager.main.GetProfilePic().EncodeToJPG());
        PlayerJoinData dataToSend = new PlayerJoinData(nameId, gamePin, stringPic, currLevel);
        io.Emit("player-join", JsonUtility.ToJson(dataToSend, true));
        uiManager.DeactivateSingleClickButton("JoinGame");
    }
    public void SendEmoji(int emojiId)
    {
        Emoji dataToSend = new Emoji(emojiId, playerManager.GetPlayerUser(io.SocketID).playerId);
        io.Emit("player-send-emoji", JsonUtility.ToJson(dataToSend, true));
    }
    public void StartGame()
    {
        int gamePin = int.Parse(DataManager.main.GetGamePin());
        string nameId = DataManager.main.GetUserName();
        int lvl = 3;
        io.Emit("test", "dgsgds");
        PlayerJoinData dataToSend = new PlayerJoinData(nameId, gamePin, "1", currLevel);
        io.Emit("host-start-bingo-game", JsonUtility.ToJson(dataToSend, true));
        ClaimNewTurn();
    }
    public void IncomingPlayer(SocketIOEvent obj)
    {
        uiManager.SetLog("entra jugador");
        Debug.Log(obj.data);
        IncomingDataPlayer[] newPlayer = JsonHelper.FromJson<IncomingDataPlayer>(fixJson(obj.data + "]"));
        //IncomingDataPlayer newPlayer = JsonUtility.FromJson<IncomingDataPlayer>(obj.data.Trim('['));
        //playerManager.CreatePlayer(newPlayer.nameId, newPlayer.playerId);
        //uiManager.UpdateLobby(playerManager.GetPlayerById(newPlayer.playerId));
        newPlayer.ToList().ForEach(p =>
        {
            if (!playerManager.PlayerAlreadyExists(p.playerId))
            {
                Texture2D newPhoto = new Texture2D(1, 1);
                if (p.profilePic != null)
                {
                    newPhoto.LoadImage(Convert.FromBase64String(p.profilePic));
                    newPhoto.Apply();
                }
                playerManager.CreatePlayer(p.nameId, p.playerId, false, newPhoto);
                if(p.playerId == io.SocketID)
                {
                    playerManager.SetCurrConsecutiveId(p.playerId);
                }
                uiManager.UpdateLobby(playerManager.GetPlayerById(p.playerId));
            }
        });
    }
    public void CheckBingoAttempt()
    {
        int gamePin = int.Parse(DataManager.main.GetGamePin());
        string nameId = "host";
        io.Emit("test", "dgsgds");
        PlayerJoinData dataToSend = new PlayerJoinData(nameId, gamePin, "1", currLevel);
        //string playerToJson = JsonHelper.ToJson(uiManager.GetBoardNumbers().ToArray(), true);
        string[] boardNumbers = uiManager.GetBoardNumbers().ToArray();
        io.Emit("player-attempt-to-win", JsonHelper.ToJson(boardNumbers, true));
        //io.Emit("player-attempt-to-win", JsonUtility.ToJson(dataToSend, true)+ JsonHelper.ToJson(uiManager.GetBoardNumbers().ToArray(), true));
    }
    string fixJson(string value)
    {
        value = "{\"Items\":" + value + "}";
        return value;
    }
    public void Received(SocketIOEvent obj)
    {
        string gamePingString = Regex.Match(obj.data, @"\d+").Value;
        Debug.Log(gamePingString);
        //int gamePin = int.Parse(resultString);
        Debug.Log("gamePin: " + gamePingString);
        uiManager.SetTextPin(gamePingString);
        DataManager.main.SaveGamePin(gamePingString);
        //Debug.Log("recibido" + obj.data.ToString().Trim('"').Trim('{', '}'));
        int gamePin = int.Parse(DataManager.main.GetGamePin());
        string nameId = DataManager.main.GetUserName();
        string stringPic = Convert.ToBase64String(DataManager.main.GetProfilePic().EncodeToJPG());
        PlayerJoinData dataToSend = new PlayerJoinData(nameId, gamePin, stringPic, currLevel);
        playerManager.CreatePlayer(nameId, io.SocketID, true, DataManager.main.GetProfilePic());
        io.Emit("player-host-join", JsonUtility.ToJson(dataToSend, true));
    }

}
