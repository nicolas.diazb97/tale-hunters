﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class emojisMenu : MonoBehaviour
{
    bool opening;
    public GameObject menuPopUp;
    // Start is called before the first frame update
    public void EmojisButton()
    {
        if (opening)
        {
            menuPopUp.SetActive(false);
            opening = false;
        }
        else
        {
            menuPopUp.SetActive(true);
            opening = true;
        }
    }
}
