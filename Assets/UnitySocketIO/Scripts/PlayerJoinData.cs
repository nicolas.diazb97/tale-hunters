﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerJoinData : MonoBehaviour
{
    [SerializeField]
    public int pin;
    [SerializeField]
    public string nameID;
    [SerializeField]
    public string profilePic;
    [SerializeField]
    public int currLevel;
    // Start is called before the first frame update
    public PlayerJoinData(string _name, int _gamePin, string _profilePic, int _currLevel)
    {
        pin = _gamePin;
        nameID = _name;
        profilePic = _profilePic;
        currLevel = _currLevel;
    }
}
