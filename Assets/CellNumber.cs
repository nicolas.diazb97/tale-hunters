﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellNumber : MonoBehaviour
{
    public bool choosen;
    Text numberText;
    public int number;
    private void OnEnable()
    {
        numberText = GetComponentInChildren<Text>(true);
    }
    // Start is called before the first frame update
    public void UpdateNumber(int _number)
    {
        numberText.text = _number.ToString();
        number = _number;
    }
}
