﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpContrato : MonoBehaviour
{
    public Animator FirstHolder;
    public GameObject PopUpOmitir;

    public void OpenPopUp()
    {
        PopUpOmitir.SetActive(true);
    }

    public void ClosePopUp()
    {
        PopUpOmitir.SetActive(false);
    }
    
    public void ClosePopUpAnimation()
    {
        PopUpOmitir.SetActive(false);
        FirstHolder.Play("Omitir");
    }
}
