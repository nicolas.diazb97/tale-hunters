﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Opciones : MonoBehaviour
{
    //Email
    public GameObject EmailButton;
    public GameObject EmailGreenButton;
    public Animator EmailGreenButtonAnimator;

    //Marketing

    public GameObject MarketingButton;
    public Animator MarketingButtonAnimator;

    public GameObject MarketingWhiteButton;

    public Animator MarketingClonAnimator;


    public GameObject MarketingRecurso;
    //Inventarios
    public Animator InventoryButtonAnimator;

    public Animator InventoryButtonForMarketingAnimator;

    public Animator Contenedor;

    // Empleados

    public Animator EmployeesButtonAnimator;
    public void TransicionCorreoAMarketing()
    {
        Contenedor.Play("Correo a Marketing");

        EmailButton.SetActive(false);
        EmailGreenButton.SetActive(true);
        EmailGreenButtonAnimator.Play("Mover Izquierda");

        MarketingButtonAnimator.Play("Traer al frente");
    }


    public void TransicionCorreoAInventario()
        {
            Contenedor.Play("Correo a Inventario");
        InventoryButtonAnimator.Play("Inventario al frente");
        }

    public void TransicionCorreoAEmpleados()
    {
        Contenedor.Play("Correo a Empleados");
    }

    public void TransicionMarketingACorreo()
    {
        Contenedor.Play("Marketing a Correo");

        EmailGreenButtonAnimator.Play("Traer al frente");

        MarketingButton.SetActive(true);
        MarketingWhiteButton.SetActive(false);
        MarketingButtonAnimator.Play("Mover Derecha");
    }

    public void TransicionMarketingAInventario()
    {
        Contenedor.Play("Marketing a Inventario");

        InventoryButtonForMarketingAnimator.Play("Inventario a correo");
    }

    public void TransicionMarketingDeEsquinaAlFrente()
    {
        Contenedor.Play("Inventario a Marketing");

        MarketingClonAnimator.Play("Marketing al frente");
    }

    public void MarketingRecursos()
    {
        Contenedor.Play("Inventario a Marketing");

        MarketingRecurso.SetActive(false);
        MarketingButton.SetActive(true);

        MarketingButtonAnimator.Play("Traer al frente Izquierda");
    }

}
