﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopBarWhites : MonoBehaviour
{
    public Animator Contenedor;

    public GameObject Correo;
    public GameObject MarketingWhite;
    public GameObject InventarioWhite;
    public GameObject EmpleadosWhite;
    public GameObject MejorasWhite;
    public GameObject SalaPrivadaWhite;

    public GameObject CorreoGreen;
    public GameObject Marketing;
    public GameObject Inventario;
    public GameObject Empleados;
    public GameObject Mejoras;
    public GameObject SalaPrivada;

    public void MarketingPrincipal()
    {
        MarketingWhite.SetActive(true);
        Correo.SetActive(false);
        InventarioWhite.SetActive(false);
        EmpleadosWhite.SetActive(false);
        MejorasWhite.SetActive(false);
        SalaPrivadaWhite.SetActive(false);

        Marketing.SetActive(false);
        CorreoGreen.SetActive(true);
        Inventario.SetActive(true);
        Empleados.SetActive(true);
        Mejoras.SetActive(true);
        SalaPrivada.SetActive(true);

        Contenedor.Play("ReGrowMarketing");
    }

    public void InventarioPrincipal()
    {
        InventarioWhite.SetActive(true);
        MarketingWhite.SetActive(false);
        Correo.SetActive(false);
        EmpleadosWhite.SetActive(false);
        MejorasWhite.SetActive(false);
        SalaPrivadaWhite.SetActive(false);

        Inventario.SetActive(false);
        Marketing.SetActive(true);
        CorreoGreen.SetActive(true);
        Empleados.SetActive(true);
        Mejoras.SetActive(true);
        SalaPrivada.SetActive(true);

        Contenedor.Play("ReGrowInventario");
    }

    public void EmpleadosPrincipal()
    {
        EmpleadosWhite.SetActive(true);
        MarketingWhite.SetActive(false);
        Correo.SetActive(false);
        InventarioWhite.SetActive(false);
        MejorasWhite.SetActive(false);
        SalaPrivadaWhite.SetActive(false);

        Empleados.SetActive(false);
        Marketing.SetActive(true);
        CorreoGreen.SetActive(true);
        Inventario.SetActive(true);
        Mejoras.SetActive(true);
        SalaPrivada.SetActive(true);

        Contenedor.Play("ReGrowEmpleados");
    }

    public void MejorasPrincipal()
    {
        MejorasWhite.SetActive(true);
        MarketingWhite.SetActive(false);
        Correo.SetActive(false);
        InventarioWhite.SetActive(false);
        EmpleadosWhite.SetActive(false);
        SalaPrivadaWhite.SetActive(false);

        Mejoras.SetActive(false);
        Marketing.SetActive(true);
        CorreoGreen.SetActive(true);
        Inventario.SetActive(true);
        Empleados.SetActive(true);
        SalaPrivada.SetActive(true);

        Contenedor.Play("ReGrowMejoras");
    }

    public void CorreoPrincipal()
    {
        Correo.SetActive(true);
        MarketingWhite.SetActive(false);
        InventarioWhite.SetActive(false);
        EmpleadosWhite.SetActive(false);
        MejorasWhite.SetActive(false);
        SalaPrivadaWhite.SetActive(false);

        CorreoGreen.SetActive(false);
        Marketing.SetActive(true);
        Inventario.SetActive(true);
        Empleados.SetActive(true);
        Mejoras.SetActive(true);
        SalaPrivada.SetActive(true);

        Contenedor.Play("ReGrowCorreo");
    }

    public void SalaPrivadaPrincipal()
    {
        SalaPrivadaWhite.SetActive(true);
        MarketingWhite.SetActive(false);
        Correo.SetActive(false);
        InventarioWhite.SetActive(false);
        EmpleadosWhite.SetActive(false);
        MejorasWhite.SetActive(false);

        SalaPrivada.SetActive(false);
        Marketing.SetActive(true);
        CorreoGreen.SetActive(true);
        Inventario.SetActive(true);
        Empleados.SetActive(true);
        Mejoras.SetActive(true);

        Contenedor.Play("ReGrowSala");
    }

    public GameObject Computer;

    public void CloseComputer()
    {
        Computer.SetActive(false);
    }
}
