﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptOmitir : MonoBehaviour
{
    public GameObject popUpOmitir;
    public GameObject no_Contracts;
    public Animator no_ContractsAnimator;

    public Animator firstContract;
    public Animator secondContract;

    int contratoCounter = 2;
    public void Animations()
    {
        GameObject omitirBoton1 = GameObject.Find("Email");
        OmitirButton omitir1 = omitirBoton1.GetComponent<OmitirButton>();

        if(omitir1.contadorOmitir == 1)
        {
            firstContract.Play("To-Left");
            popUpOmitir.SetActive(false);
            contratoCounter--;
        }

        if (omitir1.contadorOmitir == 2)
        {
            secondContract.Play("To-Left");
            popUpOmitir.SetActive(false);
            contratoCounter--;
        }

        


        if(contratoCounter == 0)
        {
            no_Contracts.SetActive(true);
            no_ContractsAnimator.Play("Grow");
        }

    }
}
