﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSlider : MonoBehaviour
{
    public Animator Line;
    public Animator ContratosYLicitaciones;

    public Animator LineEmployees;
    public Animator ContratadosYContratar;

    public GameObject DropDownButton;
    public GameObject BringUpButton;

    public void Licitaciones()
    {
        Line.Play("Right Animation");
        ContratosYLicitaciones.Play("Left Animation");

        DropDownButton.SetActive(true);
        BringUpButton.SetActive(false);
    }

    public void Contratos()
    {
        Line.Play("Left Animation");
        ContratosYLicitaciones.Play("Right Animation");
    }

    public void Contratar()
    {
        LineEmployees.Play("Move Right");
        ContratadosYContratar.Play("Left");
    }

    public void Contratados()
    {
        LineEmployees.Play("Move Left");
        ContratadosYContratar.Play("Right");
    }
}
