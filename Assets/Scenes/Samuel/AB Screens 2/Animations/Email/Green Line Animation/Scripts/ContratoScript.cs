﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContratoScript : MonoBehaviour
{

    public GameObject ContratoCamping;
    public GameObject ContratoNeucha;
    public GameObject LicitacionCamping;
    public GameObject LicitacionNeucha;


    public void RightActionFont()
    {
        ContratoCamping.SetActive(false);
        ContratoNeucha.SetActive(true);
        LicitacionNeucha.SetActive(false);
        LicitacionCamping.SetActive(true);

    }
    
    public void LeftActionFont()
    {
        ContratoCamping.SetActive(true);
        ContratoNeucha.SetActive(false);
        LicitacionNeucha.SetActive(true);
        LicitacionCamping.SetActive(false);

    }

}
