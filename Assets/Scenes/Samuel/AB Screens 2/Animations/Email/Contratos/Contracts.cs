﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contracts : MonoBehaviour
{
    public Animator White;

    public Animator Entregar;
    public Animator Omitir;

    public void DropDown()
    {
        White.Play("Drop-Down");
    }

    public void BringUp()
    {
        Entregar.Play("Shrink");
        Omitir.Play("Shrink");
    }
}
