﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmpleadosEvents : MonoBehaviour
{
    public GameObject DropDown;
    public GameObject BringUp;


    public void BringUpButton()
    {
        DropDown.SetActive(false);
        BringUp.SetActive(true);
    }

    public void DropDownButton()
    {
        BringUp.SetActive(false);
        DropDown.SetActive(true);
    }

    public GameObject DropDownContratar;
    public GameObject BringUpContratar;

    public void BringUpContratarButton()
    {
        DropDownContratar.SetActive(false);
        BringUpContratar.SetActive(true);
    }

    public void DropDownContratarButton()
    {
        BringUpContratar.SetActive(false);
        DropDownContratar.SetActive(true);
    }
}
