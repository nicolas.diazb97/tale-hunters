﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmpleadosScript : MonoBehaviour
{
    public Animator HolderEmpleado;

    public Animator ObjetosInternos;
    public Animator ObjetosInternos2;

    public void GrowAnimation()
    {
        HolderEmpleado.Play("Expand");
    }

    public void RetractAnimation()
    {
        HolderEmpleado.Play("Retract");
    }

    public void CambiarSalario()
    {
        ObjetosInternos.Play("Left1");
        ObjetosInternos2.Play("Left2");
    }

    public void Cancelar()
    {
        ObjetosInternos.Play("Right1");
        ObjetosInternos2.Play("Right2");
    }

    public void ConfirmarSalario()
    {
        ObjetosInternos.Play("Right1");
        ObjetosInternos2.Play("Right2");
    }

    public Animator HolderContratar;

    public void Expand2Animation()
    {
        HolderContratar.Play("Expand");
    }

    public void Retract2Animation()
    {
        HolderContratar.Play("Retract");
    }

    ///

    public GameObject PopUpEmpleadoDespedir;

    public void OpenPopUpEmpleadoDespedir()
    {
        PopUpEmpleadoDespedir.SetActive(true);
    }

    public void ClosePopUpEmpleadoDespedir()
    {
        PopUpEmpleadoDespedir.SetActive(false);
    }
}
