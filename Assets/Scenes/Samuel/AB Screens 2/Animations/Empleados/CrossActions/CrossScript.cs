﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossScript : MonoBehaviour
{
    public Animator empleado_A_Contratar;
    public GameObject no_Employees;

    public void EmployeeHired()
    {
        empleado_A_Contratar.Play("Delete");
        no_Employees.SetActive(false);
    }
}
