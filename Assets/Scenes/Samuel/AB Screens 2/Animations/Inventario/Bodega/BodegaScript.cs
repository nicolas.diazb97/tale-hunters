﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodegaScript : MonoBehaviour
{
    public GameObject MaterialesNeucha;
    public GameObject MaterialesCamping;
    public GameObject ProductosCamping;
    public GameObject ProductosNeucha;
    public Animator Line;
    public Animator SliderProductos;

    public void LeftActionFonts()
    {
        MaterialesNeucha.SetActive(false);
        MaterialesCamping.SetActive(true);
        ProductosCamping.SetActive(false);
        ProductosNeucha.SetActive(true);

        Line.Play("Mover Izquierda");
        SliderProductos.Play("Move-To-Materials");
    }

    public void RightActionFonts()
    {
        MaterialesNeucha.SetActive(true);
        MaterialesCamping.SetActive(false);
        ProductosCamping.SetActive(true);
        ProductosNeucha.SetActive(false);

        Line.Play("Mover Derecha");
        SliderProductos.Play("Move-To-Productos");
    }
}
