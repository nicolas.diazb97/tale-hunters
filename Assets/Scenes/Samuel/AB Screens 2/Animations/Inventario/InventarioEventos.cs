﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventarioEventos : MonoBehaviour
{
    public Animator Sillas;
    public GameObject DropDown;
    public GameObject BringUp;

    public GameObject WhiteSillas;
    public Animator Linea1;
    public Animator Linea2;
    public Animator ImagenSilla;
    public Animator TextoHolderSillas;

    public Animator SillaMDF;
    public Animator SillaPino;
    public Animator SillaGuayacan;

    public GameObject InteriorSillas;

    public void LineasBajan()
    {
        Linea1.Play("Linea-baja");
        Linea2.Play("Linea2-baja");
    }

    public void ImagenTextoDecrece()
    {
        ImagenSilla.Play("Shrink");
        InteriorSillas.SetActive(true);
        TextoHolderSillas.Play("Left");
    }

    public void ImagenTextoCrece()
    {
        ImagenSilla.Play("Grow");
        InteriorSillas.SetActive(false);    
        Sillas.Play("Bring-Up");
        DropDown.SetActive(true);
        BringUp.SetActive(false);
    }

    public void ImagenesCrecen()
    {
        SillaMDF.Play("Grow");
        SillaPino.Play("Grow");
        SillaGuayacan.Play("Grow");
        WhiteSillas.SetActive(true);
    }

    public void ImagenesDecrecen()
    {
        SillaMDF.Play("Shrink");
        SillaPino.Play("Shrink");
        SillaGuayacan.Play("Shrink");
        TextoHolderSillas.Play("Right");
    }

}
