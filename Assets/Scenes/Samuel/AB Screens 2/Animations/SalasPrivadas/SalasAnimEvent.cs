﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SalasAnimEvent : MonoBehaviour
{
    public Animator maskUsersHolder;
    public Animator objeto;
    public GameObject estado;
    public GameObject leaveButton;


    public GameObject SolicitarAcceso;
    public GameObject Cancelar;
    public Text estadoUsers;
    public GameObject solicitarAccesoGrey2;

    public void GrowMask()
    {
        maskUsersHolder.Play("Grow");
    }

    public void RetractObject()
    {
        objeto.Play("Retract");
    }

    public void ChangeButtons()
    {

        SolicitarAcceso.SetActive(true);
        Cancelar.SetActive(false);
        leaveButton.SetActive(false);
        estado.SetActive(true);

        estadoUsers.text = "Abierta";
        solicitarAccesoGrey2.SetActive(false);
    }
}
