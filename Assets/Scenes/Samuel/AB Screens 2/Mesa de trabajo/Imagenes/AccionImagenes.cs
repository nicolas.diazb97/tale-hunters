﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccionImagenes : MonoBehaviour
{
    public GameObject PurpleSillas;
    public GameObject PurpleTorres;
    public GameObject PurpleTrompos;

    public Image Sillas;
    public Image Torres;
    public Image Trompos;

    public Animator Segundo;

    public Animator MDF;
    public Animator MDFSilla;
    public Animator PinoSilla;
    public Animator GuayacanSilla;

    public Animator MDFTorre;
    public Animator PinoTorre;
    public Animator GuayacanTorre;

    public Animator MDFTrompo;
    public Animator PinoTrompo;
    public Animator GuayacanTrompo;

    public Text materialBodega;
    public Text materialInventarioMDF;
    public Text materialInventarioPino;
    public Text materialInventarioGua;
    ///

    public Animator Tercero;
    public GameObject purpleMDF;
    public GameObject purplePino;
    public GameObject purpleGua;

    public Image sillaMDF;
    public Image sillaPino;
    public Image sillaGua;

    public Image torreMDF;
    public Image torrePino;
    public Image torreGua;

    public Image trompoMDF;
    public Image trompoPino;
    public Image trompoGua;

    public Animator Cuarto;

    public GameObject purpleEmpleado;

    public Text CantidadMaterial;
    public Text MaterialTipo;

    public Text sillasText;
    public Text torresText;
    public Text tromposText;

    public Text mdfText;
    public Text pinoText;
    public Text guaText;

    int ActiveCounter = 0;

    public void PurpleSillasActive()
    {
        if (ActiveCounter <= 0)
        {
            Segundo.Play("Grow");

            Sillas.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            Torres.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
            Trompos.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

            PurpleSillas.SetActive(true);
            PurpleTorres.SetActive(false);
            PurpleTrompos.SetActive(false);

            MDFSilla.Play("Grow");
            PinoSilla.Play("Grow");
            GuayacanSilla.Play("Grow");

            CantidadMaterial.text = sillasText.text;
            SliderMesa.main.OnValueChanged(1);

        }
        else
        {
            Sillas.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            Torres.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
            Trompos.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

            PurpleSillas.SetActive(true);
            PurpleTorres.SetActive(false);
            PurpleTrompos.SetActive(false);

            MDFTorre.Play("Shrink");
            PinoTorre.Play("Shrink");
            GuayacanTorre.Play("Shrink");

            MDFTrompo.Play("Shrink");
            PinoTrompo.Play("Shrink");
            GuayacanTrompo.Play("Shrink");
         
            MDFSilla.Play("Grow");
            PinoSilla.Play("Grow");
            GuayacanSilla.Play("Grow");

            CantidadMaterial.text = sillasText.text;
            SliderMesa.main.OnValueChanged(1);
        }
        ActiveCounter++;





    }

    public void PurpleTorresActive()
    {
        if (ActiveCounter <= 0)
        {
            Segundo.Play("GrowTorres");

            Sillas.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
            Torres.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            Trompos.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

            PurpleTorres.SetActive(true);
            PurpleSillas.SetActive(false);
            PurpleTrompos.SetActive(false);

            MDFTorre.Play("Grow");
            PinoTorre.Play("Grow");
            GuayacanTorre.Play("Grow");

            CantidadMaterial.text = torresText.text;
            SliderMesa.main.OnValueChanged(1);
        }

        else 
        {
            Sillas.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
            Torres.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            Trompos.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

            PurpleTorres.SetActive(true);
            PurpleSillas.SetActive(false);
            PurpleTrompos.SetActive(false);

            MDFTrompo.Play("Shrink");
            PinoTrompo.Play("Shrink");
            GuayacanTrompo.Play("Shrink");

            MDFSilla.Play("Shrink");
            PinoSilla.Play("Shrink");
            GuayacanSilla.Play("Shrink");

            MDFTorre.Play("Grow");
            PinoTorre.Play("Grow");
            GuayacanTorre.Play("Grow");

            CantidadMaterial.text = torresText.text;
            SliderMesa.main.OnValueChanged(1);

        }
        ActiveCounter++;
    }

    public void PurpleTromposActive()
    {
        if(ActiveCounter <= 0)
        {
            Segundo.Play("GrowTrompos");

            PurpleTrompos.SetActive(true);
            PurpleTorres.SetActive(false);
            PurpleSillas.SetActive(false);

            Sillas.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
            Torres.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
            Trompos.GetComponent<Image>().color = new Color32(255, 255, 255, 255);

            MDFTrompo.Play("Grow");
            PinoTrompo.Play("Grow");
            GuayacanTrompo.Play("Grow");

            CantidadMaterial.text = tromposText.text;
            SliderMesa.main.OnValueChanged(1);
        }

        else
        {
            PurpleTrompos.SetActive(true);
            PurpleTorres.SetActive(false);
            PurpleSillas.SetActive(false);

            Sillas.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
            Torres.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
            Trompos.GetComponent<Image>().color = new Color32(255, 255, 255, 255);

            MDFTorre.Play("Shrink");
            PinoTorre.Play("Shrink");
            GuayacanTorre.Play("Shrink");

            MDFSilla.Play("Shrink");
            PinoSilla.Play("Shrink");
            GuayacanSilla.Play("Shrink");

            MDFTrompo.Play("Grow");
            PinoTrompo.Play("Grow");
            GuayacanTrompo.Play("Grow");

            CantidadMaterial.text = tromposText.text;
            SliderMesa.main.OnValueChanged(1);
        }

        ActiveCounter++;

    }

    public void OpenThirdObjectMDF()
    {
        purpleMDF.SetActive(true);
        purplePino.SetActive(false);
        purpleGua.SetActive(false);

        sillaMDF.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        sillaPino.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
        sillaGua.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

        torreMDF.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        torrePino.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
        torreGua.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

        trompoMDF.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        trompoPino.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
        trompoGua.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

        Tercero.Play("Grow");

        MaterialTipo.text = mdfText.text;
        materialBodega.text = materialInventarioMDF.text + " " + MaterialTipo.text;
    }

    public void OpenThirdObjectPino()
    {
        purplePino.SetActive(true);
        purpleMDF.SetActive(false);
        purpleGua.SetActive(false);

        sillaPino.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        sillaMDF.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
        sillaGua.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

        torrePino.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        torreMDF.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
        torreGua.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

        trompoPino.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        trompoMDF.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
        trompoGua.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

        Tercero.Play("Grow");
        MaterialTipo.text = pinoText.text;
        materialBodega.text = materialInventarioPino.text + " " + MaterialTipo.text;
    }

    public void OpenThirdObjectGua()
    {
        purpleGua.SetActive(true);
        purpleMDF.SetActive(false);
        purplePino.SetActive(false);

        sillaGua.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        sillaPino.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
        sillaMDF.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

        torreGua.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        torrePino.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
        torreMDF.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

        trompoGua.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        trompoPino.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
        trompoMDF.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

        Tercero.Play("Grow");
        MaterialTipo.text = guaText.text;
        materialBodega.text = materialInventarioPino.text + " " + MaterialTipo.text;
    }


    public GameObject clock;
    public Slider slider;
    public Image fill;
    public void PabloChosen()
    {
        Cuarto.Play("Grow");

        purpleEmpleado.SetActive(true);
        clock.SetActive(false);
        slider.interactable = false;
        fill.GetComponent<Image>().color = new Color32(200, 200, 200, 128);


        SliderMesa.main.OnValueChanged(1);
    }

    public GameObject MesaDeTrabajo;
    public GameObject PopUpMateriales;

    public void CloseMesaDeTrabajo()
    {
        MesaDeTrabajo.SetActive(false);
    }

    public void ClosePopUpMateriales()
    {
        PopUpMateriales.SetActive(false);
    }
}
