﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImagenesSegundo : MonoBehaviour
{
    public GameObject PurpleSillaM;

    public Image SillaM;
    public Image SillaP;
    public Image SillaG;

    public Animator Tercero;

    int ActiveCounter = 0;

    public void PurpleSillaMActive()
    {
        if (ActiveCounter <= 0)
        {
            Tercero.Play("Grow");

            SillaM.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            SillaP.GetComponent<Image>().color = new Color32(125, 125, 125, 178);
            SillaG.GetComponent<Image>().color = new Color32(125, 125, 125, 178);

            PurpleSillaM.SetActive(true);          

        }
        else
        {

        }
        ActiveCounter++;





    }
}
