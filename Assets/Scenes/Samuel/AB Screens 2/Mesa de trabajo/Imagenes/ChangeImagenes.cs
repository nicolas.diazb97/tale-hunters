﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeImagenes : MonoBehaviour
{
    public Image Punto1Imagen1;
    public Image Punto1Imagen2;
    public Image Punto1Imagen3;
    public Text Punto1Texto1;
    public Text Punto1Texto2;
    public Text Punto1Texto3;

    public Image Punto2SillaMDF;
    public Image Punto2SillaPino;
    public Image Punto2SillaGua;

    public Image Punto2TorresMDF;
    public Image Punto2TorresPino;
    public Image Punto2TorresGua;

    public Image Punto2TromposMDF;
    public Image Punto2TromposPino;
    public Image Punto2TromposGua;


    public Text Punto2Texto1;
    public Text Punto2Texto2;
    public Text Punto2Texto3;

    public Image purpleSillas;
    public Image purpleTorres;
    public Image purpleTrompos;
    public Image purpleMDF;
    public Image purplePino;
    public Image purpleGua;
    public Image purpleEmpleado;

    //Sprites

    //Carpintero
    public Sprite sillaPunto1;
    public Sprite torrePunto1;
    public Sprite trompoPunto1;

    public Sprite sillaMDF;
    public Sprite sillaPino;
    public Sprite sillaGua;

    public Sprite torreMDF;
    public Sprite torrePino;
    public Sprite torreGua;

    public Sprite trompoMDF;
    public Sprite trompoPino;
    public Sprite trompoGua;


    //Sastre

    public Sprite pantalonPunto1;
    public Sprite camisaPunto1;
    public Sprite chaquetaPunto1;

    public Sprite pantalonPoliester;
    public Sprite pantalonAlgodon;
    public Sprite pantalonSeda;

    public Sprite camisaPoliester;
    public Sprite camisaAlgodon;
    public Sprite camisaSeda;

    public Sprite chaquetaPoliester;
    public Sprite chaquetaAlgodon;
    public Sprite chaquetaSeda;



    //Inventario

    public Image imagenSilla;
    public Image imagenTorre;
    public Image imagenTrompo;
    public Text tituloSillas;
    public Text tituloTorres;
    public Text tituloTrompos;
    public Text descripcionSillas;
    public Text descripcionTorres;
    public Text descripcionTrompos;

    public Image interiorSillaMDF;
    public Image interiorSillaPino;
    public Image interiorSillaGua;

    public Image interiorTorreMDF;
    public Image interiorTorrePino;
    public Image interiorTorreGua;

    public Image interiorTrompoMDF;
    public Image interiorTrompoPino;
    public Image interiorTrompoGua;

    public Image materialMDF;
    public Image materialPino;
    public Image materialGua;

    public Text mdf1;
    public Text pino1;
    public Text gua1;
    public Text mdf2;
    public Text pino2;
    public Text gua2;
    public Text mdf3;
    public Text pino3;
    public Text gua3;
    public Text mdf4;
    public Text pino4;
    public Text gua4;

    public Text poliester;
    public Text algodon;
    public Text seda;

    //Sprites


    //Carpintero

    public Sprite sillaPrincipal;
    public Sprite torrePrincipal;
    public Sprite trompoPrincipal;

    public Sprite sillaMDFInventario;
    public Sprite sillaPinoInventario;
    public Sprite sillaGuaInventario;

    public Sprite torreMDFInventario;
    public Sprite torrePinoInventario;
    public Sprite torreGuaInventario;

    public Sprite trompoMDFInventario;
    public Sprite trompoPinoInventario;
    public Sprite trompoGuaInventario;

    public Sprite mdfMat;
    public Sprite pinoMat;
    public Sprite guaMat;

    //Sastre

    public Sprite camisaPrincipal;
    public Sprite pantalonPrincipal;
    public Sprite chaquetaPrincipal;

    public Sprite camisaMDFInventario;
    public Sprite camisaPinoInventario;
    public Sprite camisaGuaInventario;

    public Sprite pantalonMDFInventario;
    public Sprite pantalonPinoInventario;
    public Sprite pantalonGuaInventario;

    public Sprite chaquetaMDFInventario;
    public Sprite chaquetaPinoInventario;
    public Sprite chaquetaGuaInventario;

    public Sprite mdfMaterial;
    public Sprite pinoMaterial;
    public Sprite GuaMaterial;

    public void ChangeToCarpenterImages()
    {
        Punto1Imagen1.sprite = sillaPunto1;
        Punto1Imagen2.sprite = torrePunto1;
        Punto1Imagen3.sprite = trompoPunto1;
        Punto1Texto1.text = "Sillas";
        Punto1Texto2.text = "Torres";
        Punto1Texto3.text = "Trompos";

        Punto2Texto1.text = "MDF";
        Punto2Texto2.text = "Pino";
        Punto2Texto3.text = "Guayacán";
        descripcionSillas.text = "Aunque sencilla, esta silla es de la mas alta calidad.";
        descripcionTorres.text = "Grande, imponente y hermosa, de lo mejor.";
        descripcionTrompos.text = "Los clásicos que nunca pasan de moda.";

        Punto2SillaMDF.sprite = sillaMDF;
        Punto2SillaPino.sprite = sillaPino;
        Punto2SillaGua.sprite = sillaGua;

        Punto2TorresMDF.sprite = torreMDF;
        Punto2TorresPino.sprite = torrePino;
        Punto2TorresGua.sprite = torreGua;

        Punto2TromposMDF.sprite = trompoMDF;
        Punto2TromposPino.sprite = trompoPino;
        Punto2TromposGua.sprite = trompoGua;

        purpleSillas.GetComponent<Image>().color = new Color32(83, 60, 139, 255);
        purpleTorres.GetComponent<Image>().color = new Color32(83, 60, 139, 255);
        purpleTrompos.GetComponent<Image>().color = new Color32(83, 60, 139, 255);

        purpleMDF.GetComponent<Image>().color = new Color32(83, 60, 139, 255);
        purplePino.GetComponent<Image>().color = new Color32(83, 60, 139, 255);
        purpleGua.GetComponent<Image>().color = new Color32(83, 60, 139, 255);

        purpleEmpleado.GetComponent<Image>().color = new Color32(83, 60, 139, 255);

        //Inventario

        imagenSilla.sprite = sillaPrincipal;
        imagenTorre.sprite = torrePrincipal;
        imagenTrompo.sprite = trompoPrincipal;
        tituloSillas.text = "Sillas";
        tituloTorres.text = "Torres";
        tituloTrompos.text = "Trompos";

        interiorSillaMDF.sprite = sillaMDFInventario;
        interiorSillaPino.sprite = sillaPinoInventario;
        interiorSillaGua.sprite = sillaGuaInventario;

        interiorTorreMDF.sprite = torreMDFInventario;
        interiorTorrePino.sprite = torrePinoInventario;
        interiorTorreGua.sprite = torreGuaInventario;

        interiorTrompoMDF.sprite = trompoMDFInventario;
        interiorTrompoPino.sprite = trompoPinoInventario;
        interiorTrompoGua.sprite = trompoGuaInventario;

        materialMDF.sprite = mdfMat;
        materialPino.sprite = pinoMat;
        materialGua.sprite = guaMat;

        mdf1.text = poliester.text;
        pino1.text = algodon.text;
        gua1.text = seda.text;
        mdf2.text = poliester.text;
        pino2.text = algodon.text;
        gua2.text = seda.text;
        mdf3.text = poliester.text;
        pino3.text = algodon.text;
        gua3.text = seda.text;
        mdf4.text = poliester.text;
        pino4.text = algodon.text;
        gua4.text = seda.text;
    }

    public void ChangeToClothesImages()
    {

        Punto1Texto1.text = "Pantalones";
        Punto1Texto2.text = "Camisetas";
        Punto1Texto3.text = "Chaquetas";
        Punto1Imagen1.sprite = pantalonPunto1; 
        Punto1Imagen2.sprite = camisaPunto1; 
        Punto1Imagen3.sprite = chaquetaPunto1;

        Punto2Texto1.text = "Poliester";
        Punto2Texto2.text = "Algodon";
        Punto2Texto3.text = "Seda";
        Punto2SillaMDF.sprite = pantalonPoliester;
        Punto2SillaPino.sprite = pantalonAlgodon;
        Punto2SillaGua.sprite = pantalonSeda;

        Punto2TorresMDF.sprite = camisaPoliester;
        Punto2TorresPino.sprite = camisaAlgodon;
        Punto2TorresGua.sprite = camisaSeda;

        Punto2TromposMDF.sprite = chaquetaPoliester;
        Punto2TromposPino.sprite = chaquetaAlgodon;
        Punto2TromposGua.sprite = chaquetaSeda;

        purpleSillas.GetComponent<Image>().color = new Color32(0, 236, 163, 255);
        purpleTorres.GetComponent<Image>().color = new Color32(0, 236, 163, 255);
        purpleTrompos.GetComponent<Image>().color = new Color32(0, 236, 163, 255);

        purpleMDF.GetComponent<Image>().color = new Color32(0, 236, 163, 255);
        purplePino.GetComponent<Image>().color = new Color32(0, 236, 163, 255);
        purpleGua.GetComponent<Image>().color = new Color32(0, 236, 163, 255);

        purpleEmpleado.GetComponent<Image>().color = new Color32(0, 236, 163, 255);

        //Inventario

        imagenSilla.sprite = camisaPrincipal;
        imagenTorre.sprite = pantalonPrincipal;
        imagenTrompo.sprite = chaquetaPrincipal;
        tituloSillas.text = "Camisas";
        tituloTorres.text = "Pantalones";
        tituloTrompos.text = "Chaquetas";
        descripcionSillas.text = "Sencilla pero útil, úsala cuando quieras.";
        descripcionTorres.text = "Nada mejor que unos buenos pantalones.";
        descripcionTrompos.text = "Los clásicos que nunca pasan de moda";

        interiorSillaMDF.sprite = camisaMDFInventario;
        interiorSillaPino.sprite = camisaPinoInventario;
        interiorSillaGua.sprite = camisaGuaInventario;

        interiorTorreMDF.sprite = pantalonMDFInventario;
        interiorTorrePino.sprite = pantalonPinoInventario;
        interiorTorreGua.sprite = pantalonGuaInventario;

        interiorTrompoMDF.sprite = chaquetaMDFInventario;
        interiorTrompoPino.sprite = chaquetaPinoInventario;
        interiorTrompoGua.sprite = chaquetaGuaInventario;

        materialMDF.sprite = mdfMaterial;
        materialPino.sprite = pinoMaterial;
        materialGua.sprite = GuaMaterial;

        mdf1.text = poliester.text;
        pino1.text = algodon.text;
        gua1.text = seda.text;
        mdf2.text = poliester.text;
        pino2.text = algodon.text;
        gua2.text = seda.text;
        mdf3.text = poliester.text;
        pino3.text = algodon.text;
        gua3.text = seda.text;
        mdf4.text = poliester.text;
        pino4.text = algodon.text;
        gua4.text = seda.text;
    }
}
