﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderMesa : Singleton<SliderMesa>
{
    public Slider Slider;
    public Text Cantidad;
    public Text Costo;

    static Text TorreTexto;


    float CantidadSuma;
    float costoSuma;
    float materialesSuma;

    private void Start()
    {
        Slider.minValue = 0;
        Slider.maxValue = 10;
        Slider.wholeNumbers = true;
        Slider.value = 1;
    }
    public void OnValueChanged(float value)
    {
        GameObject imagenes = GameObject.Find("Imagenes");
        AccionImagenes accionImagenes = imagenes.GetComponent<AccionImagenes>();      
        
        //CantidadSuma = value * 10;
        Cantidad.text = value.ToString() + " " + accionImagenes.CantidadMaterial.text;

        costoSuma = value * 20;
        Costo.text = costoSuma.ToString();
        //Debug.Log("New Value" + value);
    }
}
