﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    public GameObject PopUp;
    public Text ObjectTitle;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) {

            PopUp.SetActive(true);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

                 if(Physics.Raycast(ray, out hit))

                 {
                    ObjectTitle.text = hit.transform.name;

                 }

        }

    }
}
