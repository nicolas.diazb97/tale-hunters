﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerPopUpScript : MonoBehaviour
{
    public GameObject CloseThePopUp;
    public GameObject Email;
    public GameObject Inventory;
    public GameObject Employees;
    public GameObject CompanyImprovement;
    public GameObject Market;
    public GameObject OffComputer;


    public void EmailPopUp()
    {
        Email.SetActive(true);
    }

    public void InventoryPopUp()
    {
        Inventory.SetActive(true);
    }

    public void EmployeesPopUp()
    {
        Employees.SetActive(true);
    }

    public void CompanyImprovementPopUp()
    {
        CompanyImprovement.SetActive(true);
    }
    public void MarketPopUp()
    {
        CompanyImprovement.SetActive(true);
    }

    public void ClosePopUp()
    {
        CloseThePopUp.SetActive(false);
        OffComputer.SetActive(false);
    }
}
