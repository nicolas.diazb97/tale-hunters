﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChooseScriptLooged : MonoBehaviour
{
    public Button carpenterButton;
    public Button sastreButton;

    public void DeactivateCarpenter()
    {
        carpenterButton.interactable = false;
        sastreButton.interactable = true;
    }
}

