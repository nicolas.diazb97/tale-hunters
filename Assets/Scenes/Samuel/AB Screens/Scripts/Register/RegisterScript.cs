﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RegisterScript : MonoBehaviour
{
    public void BackButton()
    {
        SceneManager.LoadScene("UserMenu");
    }

    public void Register()
    {
        SceneManager.LoadScene("ChooseLogged");
    }
}
