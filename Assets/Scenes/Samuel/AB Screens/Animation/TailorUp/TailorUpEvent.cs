﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailorUpEvent : MonoBehaviour
{
    public GameObject TailorUp;
    public GameObject Tailor;
    public void Event()
    {
        Tailor.SetActive(true);
        TailorUp.SetActive(false);
    }
}
