﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class section : MonoBehaviour
{
    public int sectionNumber;
    public GameObject playerToArrived = null;
    public bool searchForPlayer = false;
    bool playerOnChip;
    public bool isFinalSection;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public virtual void Update()
    {
        if (searchForPlayer)
        {
            //Debug.LogError("searchingforPlayer "+ Vector3.Distance(playerToArrived.transform.position, transform.position));
            if (Vector3.Distance(playerToArrived.transform.position, transform.position) <= 3.0f)
            {
                OnPlayerArrived();
            }
            else
            {
                playerOnChip = false;
            }
        }
    }
    public abstract void OnPlayerArrived();
    public void Clear()
    {
        searchForPlayer = false;
        playerOnChip = false;
    }
    public void SetPlayer(GameObject _player)
    {
        playerToArrived = _player;
    }
    public abstract void Process();
    public abstract void Init(GameObject _player);
    private void OnTrigger(Collider other)
    {
        if (other.GetComponent<PlayerRef>())
        {

        }
    }
}
