﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceMatManager : Singleton<DiceMatManager>
{
    public List<GameObject> dices;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void SetDiceMat(int _index)
    {
        UiManager.main.UiDice = dices[_index].GetComponent<Animator>();
        UiPlayer.main.Uidice = dices[_index];
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
