﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialManagerWork : Singleton<TutorialManagerWork>
{
    public TutorialScreen[] screens;
    public int currScreen = 0;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("siInicio");
        TutorialEvent();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void NextScreen()
    {
        screens[currScreen].gameObject.SetActive(false);
        currScreen++;
        TutorialEvent();
    }
    public void TutorialEvent()
    {
        screens[currScreen].Process();
    }
}
