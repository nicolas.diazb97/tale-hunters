﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailorMinigame : MonoBehaviour
{
    public GameObject gameCam;
    // Start is called before the first frame update
    void Start()
    {
    }
    private void OnEnable()
    {
        SetupCameras(false);
    }
    private void OnDisable()
    {
        SetupCameras(true);
    }
    public void SetupCameras(bool active)
    {
        gameCam.SetActive(active);
    }
}
