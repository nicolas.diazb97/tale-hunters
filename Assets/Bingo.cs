﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Bingo : MonoBehaviour
{
    public List<Letter> letters;
    public List<string> bingoNumbers;
    public List<CellNumber> cells;
    SocketManager socketManager;

    private void Start()
    {
        socketManager = FindObjectOfType<SocketManager>(); 
        letters = GetComponentsInChildren<Letter>().ToList();
        cells = GetComponentsInChildren<CellNumber>().ToList();
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void CheckForCompletedMarksOnBoard()
    {
        List<CellNumber> cellsRemaining = cells.Where(c => c.choosen == false).ToList();
        if (cellsRemaining.Count <= 0)
        {
            socketManager.CheckBingoAttempt();
        }
    }
    public List<string> GetBingoNumbers()
    {
        List<CellNumber> tempNumbers = GetComponentsInChildren<CellNumber>().ToList();
        bool queryEnded = false;
        tempNumbers.ForEach(tn =>
        {
            bingoNumbers.Add(tn.number.ToString());
            queryEnded = true;
        });
        while (!queryEnded)
        {

        }
        return bingoNumbers;
    }
    public string CheckIncomingNumber(int number)
    {
        bool isQueryReady = false;
        string matchedLetter = "";
        Debug.Log("check on bingo");
        letters.ForEach(l =>
        {
            //Debug.Log(l.name + " " + number + " " + l.NumberMatchLetter(number));
            if (l.NumberMatchLetter(number))
            {
                isQueryReady = true;
                matchedLetter = l.name;
            }
        });
        while (!isQueryReady)
        {
            Debug.Log("waiting query...");
        }
        return matchedLetter;
    }
}
