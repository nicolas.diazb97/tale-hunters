﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RefreshData 
{
    [SerializeField]
    string oldId;
    public RefreshData(string _oldId)
    {
        oldId = _oldId;
    }
}
