﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerBingoData : MonoBehaviour
{
    [SerializeField]
    public int pin;
    [SerializeField]
    public string nameID;
    [SerializeField]
    public string[] boardNumbers;
    // Start is called before the first frame update
    public PlayerBingoData(string _name, int _gamePin, string[] _boardNumbers)
    {
        pin = _gamePin;
        nameID = _name;
        boardNumbers = _boardNumbers;
    }
}
