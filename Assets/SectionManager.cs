﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SectionManager : Singleton<SectionManager>
{
    public List<section> sections;
    public Transform sectionToReturn;
    public ChipEvent OnPlayerArrived;
    // Start is called before the first frame update
    void Start()
    {
        OnPlayerArrived.AddListener((section c) =>
        {
            PlayerHasArrived(c);
        });
        sections = GetComponentsInChildren<section>().ToList();
        ActivateMeshRender(false);
        //PlayerRef test = new PlayerRef();
        //test.currSectionId = 0;
        //Destroy(SectionToMove(test, 3).gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PlayerHasArrived(section c)
    {
        c.Process();
    }
    public Transform SectionToMove(PlayerRef playerRef, int diceNumber)
    {
        bool isQueryFinished = false;
        section finalSection = sections.Where(s => s.isFinalSection == true).ToList()[0];
        List<section> matchedSections = sections.Where(s => s.sectionNumber == playerRef.currSectionId + diceNumber).ToList();
        if (matchedSections.Count > 0)
        {
            sectionToReturn = sections.Where(s => s.sectionNumber == playerRef.currSectionId + diceNumber).ToArray()[0].transform;
        }
        else
        {
            sectionToReturn = null;
        }
        List<section> matchedSections2 = sections.Where(s => s.sectionNumber == playerRef.currSectionId + diceNumber).ToList();
        if (matchedSections2.Count > 0)
        {
            sections.Where(s => s.sectionNumber == playerRef.currSectionId + diceNumber).ToArray()[0].Init(playerRef.gameObject);
            if (sections.Where(s => s.sectionNumber == playerRef.currSectionId + diceNumber).ToArray()[0].sectionNumber >= finalSection.sectionNumber)
            {
                Debug.Log("se acabo la monda");
                FindObjectOfType<SocketManager>().EndOfTheGame();
            }

        }
        return sectionToReturn;
    }
    public Transform SectionToMoveOnSpecialSection(PlayerRef playerRef, int sectionID)
    {
        bool isQueryFinished = false;
        section finalSection = sections.Where(s => s.isFinalSection == true).ToList()[0];
        List<section> matchedSections = sections.Where(s => s.sectionNumber == sectionID).ToList();
        if (matchedSections.Count > 0)
        {
            sectionToReturn = sections.Where(s => s.sectionNumber == playerRef.currSectionId + sectionID).ToArray()[0].transform;
        }
        else
        {
            sectionToReturn = null;
        }
        sections.Where(s => s.sectionNumber == sectionID).ToArray()[0].Init(playerRef.gameObject);
        if (sections.Where(s => s.sectionNumber == sectionID).ToArray()[0].sectionNumber >= finalSection.sectionNumber)
        {
            Debug.Log("se acabo la monda");
            FindObjectOfType<SocketManager>().EndOfTheGame();
        }
        return sectionToReturn;
    }
    public void ActivateMeshRender(bool state)
    {
        sections.ForEach(s =>
        {
            s.GetComponent<Renderer>().enabled = state;
        });
    }
}
