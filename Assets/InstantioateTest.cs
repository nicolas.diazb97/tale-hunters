﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantioateTest : MonoBehaviour
{
    public GameObject objToInstatiate;
    // Start is called before the first frame update
    void Start()
    {
        InstantiateObj(6);
    }
    public void InstantiateObj(int _count)
    {
        for (int i = 0; i < _count; i++)
        {
            Instantiate(objToInstatiate, transform.position, transform.rotation);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
