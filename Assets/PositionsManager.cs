﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PositionsManager : Singleton<PositionsManager>
{
    public List<UIPosition> textPositions;
    // Start is called before the first frame update
    void Start()
    {
        textPositions = GetComponentsInChildren<UIPosition>(true).ToList();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowPosition(Minigame _minigame)
    {
        GetComponentInChildren<PositionContainer>(true).gameObject.SetActive(true);
        GameResult currGameResult = PlayerManager.main.SetCurrentGamePositions(_minigame);
        for (int i = 0; i < textPositions.Count(); i++)
        {
            Debug.LogError("Tengo sueño we " + i);
            Debug.LogError("No se me ocurre nada we " + currGameResult.results.Count());
            if (i <= currGameResult.results.Count() -1)
            {
                Debug.LogError("Por favor we " + i);
                Debug.LogError("Please we " + currGameResult.results.Count());
                textPositions[i].GetComponent<Text>().text = currGameResult.results[i];

            }
        }
        StartCoroutine(WaitToClose());
    }
    public void ShowLastPosition(Minigame _minigame)
    {
        GetComponentInChildren<PositionContainer>(true).gameObject.SetActive(true);
        GetComponentInChildren<FinalButton>(true).gameObject.SetActive(true);
        GameResult currGameResult = PlayerManager.main.SetCurrentGamePositions(_minigame);
        for (int i = 0; i < textPositions.Count(); i++)
        {
            Debug.LogError("Tengo sueño we " + i);
            Debug.LogError("No se me ocurre nada we " + currGameResult.results.Count());
            if (i <= currGameResult.results.Count() - 1)
            {
                Debug.LogError("Por favor we " + i);
                Debug.LogError("Please we " + currGameResult.results.Count());
                textPositions[i].GetComponent<Text>().text = currGameResult.results[i];

            }
        }
        Time.timeScale = 0;
    }

    IEnumerator WaitToClose()
    {
        yield return new WaitForSecondsRealtime(3f);
        GetComponentInChildren<PositionContainer>(true).gameObject.SetActive(false);
    }
}
