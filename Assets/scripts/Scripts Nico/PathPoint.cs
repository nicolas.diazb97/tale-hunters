﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathPoint : MonoBehaviour
{
    public Vector2 position;
    public bool hasLeftExit;
    public bool hasRightExit;
    public bool hasUpExit;
    public bool hasDownExit;
    public List<PathPoint> blackList = new List<PathPoint>();
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Pablo>())
        {
            if (other.GetComponent<MovementManager>().state != null)
            {
                other.GetComponent<MovementManager>().state.OnPointReached(this);
            }
        }
    }
}
