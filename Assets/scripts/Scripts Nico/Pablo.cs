﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pablo : MonoBehaviour
{
    public PathPoint point;
    IDirectionState state;
    public NavMeshAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    public PathPoint currPoint()
    {
        return point;
    }
    private void Update()
    {

    }
    public void DisableMovement()
    {
        GetComponent<MovementManager>().enabled = false;
        Debug.Log("Deberia quedarse quieroooooooooooooooooooo");
    }
    public void DisablePointMovement()
    {
        GetComponent<InputManager>().tapMovementAllowed = false;
        Debug.Log("Deberia quedarse quieroooooooooooooooooooo");
    }
    public void EnablePointMovement()
    {
        GetComponent<InputManager>().tapMovementAllowed = true;
        Debug.Log("Aqui se mueveeeeeeeeeeeeeeeeeeeeeeeeeee");
    }
    public void EnableMovement()
    {
        GetComponent<MovementManager>().enabled = true;
        Debug.Log("Aqui se mueveeeeeeeeeeeeeeeeeeeeeeeeeee");
    }

}
