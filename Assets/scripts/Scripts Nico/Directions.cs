﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class Left : IDirectionState
{
    Pablo pablo;
    List<PathPoint> points;
    NavMeshAgent agent;
    float destination;
    followPlayer currCam;
    Vector3 destinationPoint;
    int moving;
    public void Init(Pablo _pablo, List<PathPoint> _points, NavMeshAgent _agent, followPlayer cam, Vector3 target)
    {
        pablo = _pablo;
        points = _points;
        agent = _agent;
        currCam = cam;
        Debug.Log("estopasHP");
        //points.ForEach(p =>
        //{
        //    Debug.Log(p.position);
        //});
        moving = 1;
        pablo.GetComponentInChildren<Animator>().SetBool("Moving", true);
        pablo.GetComponentInChildren<WalkDust>().gameObject.GetComponent<ParticleSystem>().Play();
    }

    public void Process()
    {
        destination = pablo.currPoint().position.x;
        List<PathPoint> currPoints;
        //currPoints = points.Where(p => p.position == new Vector2(destination-1, pablo.currPoint().position.y)).Where(p => p.hasLeftExit).ToList();
        currPoints = points.Where(p => p.position == new Vector2(destination - moving, pablo.currPoint().position.y)).Where(d => !pablo.currPoint().blackList.Exists(e => d)).ToList();
        currPoints.RemoveAll(c => pablo.currPoint().blackList.ToList().Exists(n => n == c));
        destinationPoint = (currPoints.Count() > 0) ? currPoints.ToArray()[0].transform.position : new Vector3(pablo.point.position.x, pablo.point.position.y, 0);
        currCam.OnPlayerMoving(pablo.gameObject);
        //Debug.Log(pablo.currPoint().position.x + " " + pablo.currPoint().position.y);
        //Debug.Log((points.Where(p => p.position == new Vector2(pablo.currPoint().position.x - 1, pablo.currPoint().position.y)).ToArray()[0]).transform.name);
        agent.SetDestination(destinationPoint);
        //if(points.Any(p => p.position == new Vector2(6.0f, 0.0f)))
        //{
        //}
        //points.ForEach(e =>
        //{
        //    Debug.Log(e.position);
        ////});
        //points.Where(p => p.position.y == pablo.currPoint().position.y).Min(p=> p.position.x)
        //IEnumerable<PathPoint> dest = points.Where(p => p.position == new Vector2(pablo.currPoint().position.x - 1, pablo.currPoint().position.y));
        //Debug.Log(dest.ToArray()[0]);
    }
    public void OnPointReached(PathPoint pointReached)
    {
        Debug.Log("esto pasa");
        if (pointReached.hasLeftExit == false)
        {
            moving = 0;
            pablo.GetComponentInChildren<Animator>().SetBool("Moving", false);
            pablo.GetComponentInChildren<WalkDust>().gameObject.GetComponent<ParticleSystem>().Stop();
        }
        pablo.point = pointReached;
    }

}
public class Right : IDirectionState
{
    Pablo pablo;
    List<PathPoint> points;
    NavMeshAgent agent;
    float destination;
    followPlayer currCam;
    Vector3 destinationPoint;
    int moving;
    bool onBlackList;
    public void Init(Pablo _pablo, List<PathPoint> _points, NavMeshAgent _agent, followPlayer cam, Vector3 target)
    {
        pablo = _pablo;
        points = _points;
        agent = _agent;
        currCam = cam;
        //points.ForEach(p =>
        //{
        //    Debug.Log(p.position);
        //});
        moving = 1;
        pablo.GetComponentInChildren<Animator>().SetBool("Moving", true);
        pablo.GetComponentInChildren<WalkDust>().gameObject.GetComponent<ParticleSystem>().Play();
    }

    public void Process()
    {
        destination = pablo.currPoint().position.x;
        List<PathPoint> currPoints;
        //currPoints = points.Where(p => p.position == new Vector2(destination-1, pablo.currPoint().position.y)).Where(p => p.hasLeftExit).ToList();
        currPoints = points.Where(p => p.position == new Vector2(destination + moving, pablo.currPoint().position.y)).ToList().Where(d => !pablo.currPoint().blackList.Exists(e => d)).ToList();
        currPoints.RemoveAll(c => pablo.currPoint().blackList.ToList().Exists(n => n == c));
        destinationPoint = (currPoints.Count() > 0) ? currPoints.ToArray()[0].transform.position : new Vector3(pablo.point.position.x, pablo.point.position.y, 0);
        currCam.OnPlayerMoving(pablo.gameObject);
        //Debug.Log(pablo.currPoint().position.x + " " + pablo.currPoint().position.y);
        //Debug.Log((points.Where(p => p.position == new Vector2(pablo.currPoint().position.x - 1, pablo.currPoint().position.y)).ToArray()[0]).transform.name);
        agent.SetDestination(destinationPoint);
        //if(points.Any(p => p.position == new Vector2(6.0f, 0.0f)))
        //{
        //}
        //points.ForEach(e =>
        //{
        //    Debug.Log(e.position);
        ////});
        //points.Where(p => p.position.y == pablo.currPoint().position.y).Min(p=> p.position.x)
        //IEnumerable<PathPoint> dest = points.Where(p => p.position == new Vector2(pablo.currPoint().position.x - 1, pablo.currPoint().position.y));
        //Debug.Log(dest.ToArray()[0]);
    }
    public void OnPointReached(PathPoint pointReached)
    {
        if (pointReached.hasRightExit == false)
        {
            moving = 0;
            pablo.GetComponentInChildren<Animator>().SetBool("Moving", false);
            pablo.GetComponentInChildren<WalkDust>().gameObject.GetComponent<ParticleSystem>().Stop();
        }
        pablo.point = pointReached;
    }

}
public class Up : IDirectionState
{
    Pablo pablo;
    List<PathPoint> points;
    NavMeshAgent agent;
    float destination;
    followPlayer currCam;
    Vector3 destinationPoint;
    int moving;
    bool onBlackList;
    public void Init(Pablo _pablo, List<PathPoint> _points, NavMeshAgent _agent, followPlayer cam, Vector3 target)
    {
        pablo = _pablo;
        points = _points;
        agent = _agent;
        currCam = cam;
        //points.ForEach(p =>
        //{
        //    Debug.Log(p.position);
        //});
        moving = 1;
        pablo.GetComponentInChildren<Animator>().SetBool("Moving", true);
        pablo.GetComponentInChildren<WalkDust>().gameObject.GetComponent<ParticleSystem>().Play();
    }

    public void Process()
    {
        destination = pablo.currPoint().position.y;
        List<PathPoint> currPoints;
        //currPoints = points.Where(p => p.position == new Vector2(destination-1, pablo.currPoint().position.y)).Where(p => p.hasLeftExit).ToList();
        currPoints = points.Where(p => p.position == new Vector2(pablo.currPoint().position.x, destination + moving)).ToList();
        //PathPoint[] intersected = currPoints.Intersect(pablo.currPoint().blackList).ToArray();
        //PathPoint[] inCommon = (intersected.Length>0)? intersected
        //currPoints.Remove(currPoints.Intersect(pablo.currPoint().blackList).ToArray());
        currPoints.RemoveAll(c => pablo.currPoint().blackList.ToList().Exists(n => n == c));
        destinationPoint = (currPoints.Count() > 0) ? currPoints.ToArray()[0].transform.position : new Vector3(pablo.point.position.x, pablo.point.position.y, 0);
        currCam.OnPlayerMoving(pablo.gameObject);
        //Debug.Log(pablo.currPoint().position.x + " " + pablo.currPoint().position.y);
        //Debug.Log((points.Where(p => p.position == new Vector2(pablo.currPoint().position.x - 1, pablo.currPoint().position.y)).ToArray()[0]).transform.name);
        agent.SetDestination(destinationPoint);
        //if(points.Any(p => p.position == new Vector2(6.0f, 0.0f)))
        //{
        //}
        //points.ForEach(e =>
        //{
        //    Debug.Log(e.position);
        ////});
        //points.Where(p => p.position.y == pablo.currPoint().position.y).Min(p=> p.position.x)
        //IEnumerable<PathPoint> dest = points.Where(p => p.position == new Vector2(pablo.currPoint().position.x - 1, pablo.currPoint().position.y));
        //Debug.Log(dest.ToArray()[0]);
    }
    public void OnPointReached(PathPoint pointReached)
    {
        if (pointReached.hasUpExit == false)
        {
            moving = 0;
            pablo.GetComponentInChildren<Animator>().SetBool("Moving", false);
            pablo.GetComponentInChildren<WalkDust>().gameObject.GetComponent<ParticleSystem>().Stop();
        }
        pablo.point = pointReached;
    }

}
public class Down : IDirectionState
{
    Pablo pablo;
    List<PathPoint> points;
    NavMeshAgent agent;
    float destination;
    followPlayer currCam;
    Vector3 destinationPoint;
    int moving;
    public void Init(Pablo _pablo, List<PathPoint> _points, NavMeshAgent _agent, followPlayer cam, Vector3 target)
    {
        pablo = _pablo;
        points = _points;
        agent = _agent;
        currCam = cam;
        //points.ForEach(p =>
        //{
        //    Debug.Log(p.position);
        //});
        moving = 1;
        pablo.GetComponentInChildren<Animator>().SetBool("Moving", true);
        pablo.GetComponentInChildren<WalkDust>().gameObject.GetComponent<ParticleSystem>().Play();
    }

    public void Process()
    {
        destination = pablo.currPoint().position.y;
        List<PathPoint> currPoints;
        //currPoints = points.Where(p => p.position == new Vector2(destination-1, pablo.currPoint().position.y)).Where(p => p.hasLeftExit).ToList();
        currPoints = points.Where(p => p.position == new Vector2(pablo.currPoint().position.x, destination - moving)).ToList().Where(d => !pablo.currPoint().blackList.Exists(e => d)).ToList();
        currPoints.RemoveAll(c => pablo.currPoint().blackList.ToList().Exists(n => n == c));
        destinationPoint = (currPoints.Count() > 0) ? currPoints.ToArray()[0].transform.position : new Vector3(pablo.point.position.x, pablo.point.position.y, 0);
        currCam.OnPlayerMoving(pablo.gameObject);
        //Debug.Log(pablo.currPoint().position.x + " " + pablo.currPoint().position.y);
        //Debug.Log((points.Where(p => p.position == new Vector2(pablo.currPoint().position.x - 1, pablo.currPoint().position.y)).ToArray()[0]).transform.name);
        agent.SetDestination(destinationPoint);
        //if(points.Any(p => p.position == new Vector2(6.0f, 0.0f)))
        //{
        //}
        //points.ForEach(e =>
        //{
        //    Debug.Log(e.position);
        ////});
        //points.Where(p => p.position.y == pablo.currPoint().position.y).Min(p=> p.position.x)
        //IEnumerable<PathPoint> dest = points.Where(p => p.position == new Vector2(pablo.currPoint().position.x - 1, pablo.currPoint().position.y));
        //Debug.Log(dest.ToArray()[0]);
    }
    public void OnPointReached(PathPoint pointReached)
    {
        if (pointReached.hasDownExit == false)
        {
            moving = 0;
            pablo.GetComponentInChildren<Animator>().SetBool("Moving", false);
            pablo.GetComponentInChildren<WalkDust>().gameObject.GetComponent<ParticleSystem>().Stop();
        }
        pablo.point = pointReached;
    }

}
public class Directionated : IDirectionState
{
    Pablo pablo;
    List<PathPoint> points;
    NavMeshAgent agent;
    float destination;
    followPlayer currCam;
    Vector3 destinationPoint;
    int moving;
    bool onBlackList;
    public void Init(Pablo _pablo, List<PathPoint> _points, NavMeshAgent _agent, followPlayer cam, Vector3 target)
    {
        pablo = _pablo;
        currCam = cam;
        agent = _agent;
        destinationPoint = target;
        pablo.GetComponentInChildren<Animator>().SetBool("Moving", true);
        pablo.GetComponentInChildren<WalkDust>().gameObject.GetComponent<ParticleSystem>().Play();
    }

    public void Process()
    {
        agent.isStopped = false;
        currCam.OnPlayerMoving(pablo.gameObject);
        agent.SetDestination(destinationPoint);
        //if(points.Any(p => p.position == new Vector2(6.0f, 0.0f)))
        //{
        //}
        //points.ForEach(e =>
        //{
        //    Debug.Log(e.position);
        ////});
        //points.Where(p => p.position.y == pablo.currPoint().position.y).Min(p=> p.position.x)
        //IEnumerable<PathPoint> dest = points.Where(p => p.position == new Vector2(pablo.currPoint().position.x - 1, pablo.currPoint().position.y));
        //Debug.Log(dest.ToArray()[0]);
    }
    public void OnPointReached(PathPoint pointReached)
    {

    }

}
