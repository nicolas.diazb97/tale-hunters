﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementManager : MonoBehaviour
{
    public List<PathPoint> points = new List<PathPoint>();
    public Points pathPoints;
    public followPlayer camera;
    Pablo pablo;
    public GameObject arrow;
    public IDirectionState state;
    // Start is called before the first frame update
    void Start()
    {
        pablo = GetComponent<Pablo>();
        points = pathPoints.points;
        pablo.point = FindClosestPoint();
    }

    // Update is called once per frame
    public void FindNextTargetPoint()
    {

    }
    // Update is called once per frame
    public void SetDirectionState(IDirectionState _state)
    {
        points = pathPoints.points;
        state = _state;
        state.Init(pablo,points,pablo.agent, camera, Vector3.zero);
    }
    public void SetDirectionState(IDirectionState _state, Vector3 target)
    {
        GameObject tempPathPoint = Instantiate(arrow,target,Quaternion.Euler(new Vector3(-90,0,0)));
        Destroy(tempPathPoint, 1f);
        points = pathPoints.points;
        state = _state;
        state.Init(pablo, points, pablo.agent, camera, target);
    }
    private void Update()
    {
        if (state != null)
        {
            state.Process();
        }
    }
    public PathPoint FindClosestPoint()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("points");
        PathPoint closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go.GetComponent<PathPoint>();
                distance = curDistance;
            }
        }
        return closest;
    }
}
