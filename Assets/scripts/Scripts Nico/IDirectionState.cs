﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public interface IDirectionState
{
    void Init(Pablo pablo, List<PathPoint> points, NavMeshAgent agent, followPlayer cam, Vector3 target);
    void Process();
    void OnPointReached(PathPoint pointReached);
}
