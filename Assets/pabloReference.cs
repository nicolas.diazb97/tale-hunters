﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pabloReference : Singleton<pabloReference>
{
    public GameObject pablo;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(pablo.transform.position.x, pablo.transform.position.y, pablo.transform.position.z);
    }
}
