﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Letter : MonoBehaviour
{
    public List<CellNumber> cells;
     Bingo bingoManager;
    public bool finished;
    public int from, to;
    public List<string> numbersOnLetter = new List<string>();
    private void Start()
    {
        InitBingo();
    }
    public void InitBingo()
    {
        cells = GetComponentsInChildren<CellNumber>(true).ToList();
        cells.ForEach(c =>
        {
            c.GetComponent<Button>().onClick.AddListener(() => OnCellClicked(c));
            numbersOnLetter.Add(c.GetComponentInChildren<Text>().text);
        });
        bingoManager = FindObjectOfType<Bingo>();
        SetNumbers();
    }
    public bool NumberMatchLetter(int _number)
    {
        if (_number >= from && _number <= to)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public List<string> GetNumbers()
    {
        return numbersOnLetter;
    }
    public void OnCellClicked(CellNumber cell)
    {
        if (cell.choosen)
        {
            cell.GetComponent<Image>().color = Color.white;
            cell.choosen = false;
        }
        else
        {
            cell.GetComponent<Image>().color = Color.red;
            cell.choosen = true;
            bingoManager.CheckForCompletedMarksOnBoard();
        }
    }
    public void SetNumbers()
    {
        cells.ForEach(c =>
        {
            bool randNumIntoBoard = true;
            while (randNumIntoBoard)
            {
                int tempRandNumber = Random.Range(from, to + 1);
                List<CellNumber> cellsToCheck = cells.Where(c2 => c2.number == tempRandNumber).ToList();
                if (cellsToCheck.Count() == 0)
                {
                    Debug.Log("Aproved " + tempRandNumber);
                    c.UpdateNumber(tempRandNumber);
                    randNumIntoBoard = false;
                }
            }
        });
    }
}
