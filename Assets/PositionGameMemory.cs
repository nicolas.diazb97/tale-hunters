﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PositionGameMemory
{
    [SerializeField]
    public string playerId;
    [SerializeField]
    public int points;
    [SerializeField]
    public int position;
}
