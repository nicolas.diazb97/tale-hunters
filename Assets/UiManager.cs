﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class UiManager : Singleton<UiManager>
{
    public GameObject uiCanvas;
    public Text log;
    public Text chatLog;
    public Text bingoNumbersLog;
    public Text gamePin;
    public InputField inputGamePin;
    public GameObject createContainer;
    public GameObject pinContainer;
    public GameObject joinContainer;
    public GameObject joinWaitingContainer;
    public GameObject lobbyTextContainer;
    public ButtonContainer specialButtonsContainer;
    public List<LobbyPlayerText> textPlayersLobby;
    public List<SingleClickButtons> singleClickButtons;
    public GameObject bingoContainer;
    public GameObject winnerContainer;
    public Bingo bingo;
    public bool isPlaying;
    public SocketManager socketManager;
    public GameObject logoContainer;
    public Button playButton;
    public GameObject dice;
    public Text turnText;
    public UiPlayer uiTurn;
    public Animator UiDice;
    public Animator bookAnim;
    public GameObject book;
    public GameObject diceCanvas;
    public GameObject miniGameReference;
    public GameObject[] objsToDeactivateOnGameStarted;

    // Start is called before the first frame update
    private void Start()
    {
        socketManager = FindObjectOfType<SocketManager>();
        specialButtonsContainer = FindObjectOfType<ButtonContainer>();
        singleClickButtons = specialButtonsContainer.GetComponentsInChildren<SingleClickButtons>().ToList();
        textPlayersLobby = FindObjectsOfType<LobbyPlayerText>().ToList();
        lobbyTextContainer.SetActive(false);
    }
    public void DiceAnim(string _triggerName)
    {
        Debug.LogError(_triggerName + "ANIMACION AQUIIIIIIII");
        UiDice.Play(_triggerName);
    }
    public void InitMiniGame(Player _playerToPlay)
    {
        socketManager.ClearIntervalFull();
        //PlayerManager.main.playerOnTurn = miniGameReference;
        //pabloReference.main.pablo = miniGameReference;
        MinigameController.main.Init(_playerToPlay);
    }
    public void ToBoard()
    {
        diceCanvas.SetActive(true);

    }
    public void EndMiniGame()
    {
        diceCanvas.SetActive(false);
        socketManager.ClaimNewTurn();
    }
    public void SetWinner(string winnerName)
    {
        winnerContainer.SetActive(true);
        winnerContainer.GetComponentInChildren<Text>().text = winnerName;
    }

    public void SetTurn(string playerId, string socketId)
    {
        uiTurn.StartTurn("Turno de: " + PlayerManager.main.GetPlayerById(playerId).playerName, false);
        if (playerId == socketId)
        {
            uiTurn.StartTurn(true);
        }
    }
    public void SetDice(string number)
    {
        uiTurn.ShowDice(number);
    }
    public void ActivateBoard()
    {
        objsToDeactivateOnGameStarted.ToList().ForEach(obj =>
        {
            obj.SetActive(false);
        });
        book.SetActive(true);
        logoContainer.SetActive(false);
        bingoContainer.SetActive(true);
        lobbyTextContainer.SetActive(false);
        socketManager.CallBackPlayerIsOnGame();
        uiCanvas.SetActive(false);
        bookAnim.SetTrigger("start");
    }
    public void PlayerTurn()
    {
        dice.SetActive(true);
    }
    public List<string> GetBoardNumbers()
    {
        return bingo.GetBingoNumbers();
    }
    //public void GetBoardNumbers()
    //{
    //    bingo.GetBingoNumbers().ForEach(bn =>
    //    {
    //        Debug.Log(bn);
    //    });
    //    Debug.Log(bingo.GetBingoNumbers());
    //}
    public void SetTextPin(string pin)
    {
        //createContainer.SetActive(false);
        pinContainer.SetActive(true);
        gamePin.text = pin;
    }
    public void DeactivateSingleClickButton(string nameId)
    {
        //singleClickButtons.Where(t => t.nameId == nameId).ToArray()[0].GetComponent<Button>().interactable = false;
    }
    public string GetInputGamePin()
    {
        return inputGamePin.text;
    }
    public void SetLog(string data)
    {
        log.text = data;
    }
    public void SetChatLog(string data)
    {
        chatLog.text = data;
        StartCoroutine(WaitToClearChatLog());
    }
    public IEnumerator WaitToClearChatLog()
    {
        yield return new WaitForSecondsRealtime(3f);
        chatLog.text = "";
    }
    public void SetLogBingoNumbers(string data)
    {
        bingoNumbersLog.transform.parent.GetComponentInChildren<Image>(true).gameObject.SetActive(true);
        Debug.Log("check on uimanager");
        log.text = "";
        string oldNumbers = bingoNumbersLog.text;
        BallotsManager.main.NewBallot(bingo.CheckIncomingNumber(int.Parse(data)), data);
        //bingoNumbersLog.text = bingo.CheckIncomingNumber(int.Parse(data)) + data + " " + oldNumbers;
        //bingo.CheckIncomingNumber(int.Parse(data));
    }
    public void ClearLogBingoNumbers()
    {
        bingoNumbersLog.transform.parent.GetComponentInChildren<Image>(true).gameObject.SetActive(true);
        bingoNumbersLog.text = "";
    }
    public void UpdateLobby(Player activePyer)
    {
        lobbyTextContainer.SetActive(true);
        LobbyPlayerText availableText = textPlayersLobby.Where(t => t.state == true).Take(1).ToArray()[0];
        availableText.GetComponent<Text>().text = activePyer.playerName;
        availableText.state = false;
    }
    public void PlayerFoundGame()
    {
        joinContainer.SetActive(false);
        joinWaitingContainer.SetActive(true);
    }
}
