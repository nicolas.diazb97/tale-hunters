﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameChip : section
{
    public override void Init(GameObject _player)
    {
        playerToArrived = _player;
        searchForPlayer = true;
    }

    public override void OnPlayerArrived()
    {
        Debug.LogError("playerArrived");
        SectionManager.main.PlayerHasArrived(this.GetComponent<section>());
    }

    public override void Process()
    {
        Debug.LogError("process"+playerToArrived.name);
        PlayerManager.main.PlayAnimation(playerToArrived.GetComponent<PlayerRef>().player.playerId, "Stop");
        Clear();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }
}
