﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pointMovementTutorial : TutorialScreen
{
    public GameObject animation;
    int eventsCounter;
    Pablo pablo;
    MovementManager movementManager;
    InputManager inputManager;
    public Button nxtButton;
    bool once = true;
    // Start is called before the first frame update
    private void Start()
    {
        movementManager = FindObjectOfType<MovementManager>();
    }
    public override void Process()
    {
        pablo = FindObjectOfType<Pablo>();
        nxtButton.interactable = false;
        if (eventsCounter == 2)
        {
            TutorialManager.main.NextScreen();
        }
        if (eventsCounter == 1)
        {
            animation.SetActive(false);
            eventsCounter++;
        }
        if (eventsCounter <= 0)
        {
            this.gameObject.SetActive(true);
            eventsCounter++;
            Debug.Log("pasaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            StartCoroutine(WaitToActivate());
        }
    }
    IEnumerator WaitToActivate()
    {
        yield return new WaitForSecondsRealtime(1f);
        pablo.EnablePointMovement();
    }
    private void Update()
    {
        if (movementManager.state is Directionated)
        {
            if (once)
            {
                TutorialManager.main.TutorialEvent();
                nxtButton.interactable = true;
                once = false;
            }
        }
    }
}
