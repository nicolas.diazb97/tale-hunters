﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerRef : MonoBehaviour
{
    public int currSectionId;
    public Player player;
    public NavMeshAgent agent;
    public List<ChoosablePlayer> playableModels;

    public void Init(int _section, Player _player)
    {
        currSectionId = _section;
        player = _player;
        agent = GetComponent<NavMeshAgent>();
        DiceManager.main.RefreshPlayers();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void MovePlayer(Transform aim)
    {
        if (aim)
        {

            agent.SetDestination(aim.position);
        }
    }
    public void HasArrived()
    {

    }
}
