﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MiniGameHideaway
{
    [SerializeField]
    public string operation;
    [SerializeField]
    public int tower;
    [SerializeField]
    public int castle;
    [SerializeField]
    public int pit;
    [SerializeField]
    public int rock;
}
